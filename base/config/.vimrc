set number
syntax on
set nocompatible
set ignorecase
set smartcase
set smarttab
set backspace=indent,eol,start
set splitright
set splitbelow
set hlsearch
set incsearch
set showmatch
set cursorline
set pastetoggle=<F2>

nnoremap <silent> <C-o> :nohls<CR>

set scrolloff=2
set softtabstop=4
set shiftwidth=4
set expandtab

filetype plugin indent on
set autoindent
