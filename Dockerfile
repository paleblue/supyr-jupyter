FROM supyr-jupyter-base

WORKDIR /opt
SHELL ["/bin/bash", "-c"]

### Javascript and TypeScript
RUN npm install -g ijavascript itypescript && \
    ijsinstall && \
    its --install=global
	
### Scilab
ENV SCI_DISABLE_TK True
ENV SCILAB_EXECUTABLE /usr/bin/scilab-adv-cli
RUN mkdir /usr/share/man/man1 && \
    conda install -y scipy && \
    conda install -y -c conda-forge ipyleaflet && \
    pip install -e git+https://github.com/bqplot/bqplot.git@0.12.12#egg=bqplot && \
    pip install networkx sympy sklearn scikit-image && \
    jupyter labextension install jupyter-leaflet bqplot && \
    jupyter labextension update --all && \
    jupyter lab build && \
    echo "c.ScilabKernel.plot_settings = dict(format='svg')" > /root/.jupyter/scilab_kernel_config.py && \
    wget https://www.scilab.org/download/6.1.0/scilab-6.1.0.bin.linux-x86_64.tar.gz && \
    tar xvf scilab-6.1.0.bin.linux-x86_64.tar.gz && \
    ln -s /opt/scilab-6.1.0/bin/scilab* /usr/bin/ && \
    git clone https://github.com/Calysto/scilab_kernel.git && \
    pushd scilab_kernel && \
	pip install metakernel && \
	pip install -e . && \
	python -m scilab_kernel install && \
    popd

### Octave
RUN cd /opt && \
    apt-get install -y octave liboctave-dev && \
    pip install octave_kernel && \
    git clone https://github.com/Calysto/octave_kernel.git && \
    pushd /opt/octave_kernel && \
	python -m octave_kernel install && \
    popd

### Lua
RUN cd /opt && \
    apt-get install -y luarocks luajit && \
    luarocks install lua-llthreads2 && \
    luarocks install lzmq && \
    git clone https://github.com/guysv/ilua.git && \
    pushd /opt/ilua && \
	pip install jupyter_console==6.1.0 && \
	pip install service_identity && \
	pip install -e . && \
	python setup.py install_data -d ~/.local && \
	sed -i "s/__version__/version/g" /opt/ilua/ilua/version.py && \
    popd

### Julia
RUN cd /opt && \
    echo 'using Pkg' > /opt/julia_precompile.jl && \
    echo 'Pkg.update()' >> /opt/julia_precompile.jl && \
    echo 'Pkg.build()' >> /opt/julia_precompile.jl && \
    echo 'for pkg in keys(Pkg.installed())' >> /opt/julia_precompile.jl && \
    echo '	try' >> /opt/julia_precompile.jl && \
    echo '	    @info("Compiling $pkg")' >> /opt/julia_precompile.jl && \
    echo '	    eval(Expr(:toplevel, Expr(:using, Symbol(pkg))))' >> /opt/julia_precompile.jl && \
    echo '	    println("")' >> /opt/julia_precompile.jl && \
    echo '	catch err' >> /opt/julia_precompile.jl && \
    echo '	    @warn("Unable to precompile: $pkg")' >> /opt/julia_precompile.jl && \
    echo '	    @warn(err)' >> /opt/julia_precompile.jl && \
    echo '	    println("")' >> /opt/julia_precompile.jl && \
    echo '	end' >> /opt/julia_precompile.jl && \
    echo 'end' >> /opt/julia_precompile.jl && \
    apt-get install -y julia && \
    export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu/julia && \
    julia -e 'using Pkg; Pkg.add("IJulia")' && \
    julia -e 'using Pkg; Pkg.add("Plots"); Pkg.add("GR")' && \
    julia -e 'using Pkg; Pkg.add("DataFrames"); Pkg.add("CSV")' && \
    julia /opt/julia_precompile.jl && \
    sed -i -e 's/"env":.*/"env": {/' \
      -e '/"env":/r'<(echo '    "LD_LIBRARY_PATH": "/usr/lib/x86_64-linux-gnu/julia"'; echo '  },') -- \
      /root/.local/share/jupyter/kernels/julia-1.0/kernel.json

### Ruby
RUN cd /opt && \
    apt-get update --allow-releaseinfo-change && \
    apt-get install -y ruby ruby-dev libffi-dev libczmq-dev --fix-missing && \
    gem install ffi-rzmq && \
    gem install cztop && \
    gem install iruby --pre && \
    iruby register --force

### SageMath
ENV SAGE_ROOT /opt/sage
RUN cd /opt && \
    mkdir -p /opt/conda/share/jupyter/kernels/sagemath && \
    echo '{' > /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '  "argv": [' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	    "/opt/sage/sage",' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	    "--python",' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	    "-m",' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	    "sage.repl.ipython_kernel",' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	    "-f",' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	    "{connection_file}"' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	],' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	"display_name": "SageMath 9.0",' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	"language": "sage",' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	"env": {' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	    "PYTHONPATH": "/opt/sage/local/lib/python3.7/site-packages",' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	    "SAGE_ROOT": "/opt/sage",' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	    "SAGE_LOCAL": "/opt/sage/local",' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	    "SAGE_PKGS": "/opt/sage/local/var/lib/sage/installed",' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	    "MATHJAX_DIR": "/opt/sage/local/lib/python3.7/site-packages/notebook/static/components/MathJax"' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '	}' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    echo '}' >> /opt/conda/share/jupyter/kernels/sagemath/kernel.json && \
    apt-get install -y bc libncurses5-dev coinor-libcbc-dev libcdd-tools cliquer libcliquer-dev libec-dev eclib-tools gmp-ecm libecm-dev fflas-ffpack libgf2x-dev gfan libgivaro-dev pari-gp2c libiml-dev libisl-dev lcalc liblfunction-dev libatomic-ops-dev libbraiding-dev libgd-dev liblrcalc-dev libm4rie-dev libmpc-dev libmpfi-dev nauty pari-gp2c libpari-dev pari-galpol libxml-libxslt-perl libxml-writer-perl libperl-dev libfile-slurp-perl libjson-perl libsvg-perl libterm-readkey-perl libterm-readline-gnu-perl libmongodb-perl libterm-readline-gnu-perl libplanarity-dev planarity libppl-dev ppl-dev librw-dev libsymmetrica2-dev tachyon && \
    wget https://gmplib.org/download/gmp/gmp-6.1.2.tar.xz && \
    tar xvf gmp-6.1.2.tar.xz && \
    pushd gmp-6.1.2 && \
      ./configure --prefix=/usr \
          --includedir=/usr/include \
          --enable-cxx --enable-static=yes --enable-shared=yes && \
      make -j8 install && \
    popd && \
    wget https://gforge.inria.fr/frs/download.php/file/36224/ecm-7.0.4.tar.gz && \
    tar xvf ecm-7.0.4.tar.gz && \
    pushd ecm-7.0.4 && \
      ./configure --with-gmp-include=/usr/include \
          --with-gmp-lib=/usr/lib \
          --with-gmp=/usr \
          --enable-static=yes \
          --enable-shared=yes && \
      make -j8 install && \
    popd && \
    git clone --branch 9.1 git://trac.sagemath.org/sage.git && \
    pushd sage && \
      make -j8 configure && \
      ./configure \
          --enable-build-as-root \
          --enable-pysingular=yes \
          --disable-python2 \
          --disable-pandoc \
          --with-python=3 \
          --with-system-flint=no \
          --with-system-flintqs=no && \
          # --enable-d3js \
      make -j8 build && \
      ln -s $SAGE_ROOT/sage /usr/bin/sage && \
      ./sage -f ipython && \
      make -j8 build && \
      cp /opt/sage/local/share/jupyter/kernels/sagemath/logo-64x64.png /opt/conda/share/jupyter/kernels/sagemath/logo-64x64.png && \
    popd

### FSharp
### CSharp
ENV PATH /root/.dotnet/tools:$PATH
ENV DOTNET_CLI_TELEMETRY_OPTOUT true
RUN cd /opt && \
    wget https://packages.microsoft.com/config/ubuntu/19.10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb && \
    dpkg -i packages-microsoft-prod.deb && \
    apt-get update --allow-releaseinfo-change && \
    apt-get install -y apt-transport-https && \
    apt-get install -y dotnet-sdk-3.1 && \
    dotnet tool install -g Microsoft.dotnet-interactive && \
    dotnet interactive jupyter install
COPY ./icons/csharp/ /root/.local/share/jupyter/kernels/.net-csharp/
COPY ./icons/fsharp/ /root/.local/share/jupyter/kernels/.net-fsharp/

### R
RUN cd /opt && \
    apt-get install -y r-base libcurl4-openssl-dev libssl-dev && \
    echo "install.packages(c('repr', 'IRdisplay', 'IRkernel', 'tidyverse', 'rmarkdown', 'httr', 'shinydashboard', 'leaflet'), type='source')" | R --no-save && \
    echo "IRkernel::installspec(user = FALSE)" | R --no-save

### Elixir
ENV PATH $PATH:/root/.cache/rebar3/bin:/root/.mix
RUN cd /opt && \
    apt-get install -y erlang erlang-dev erlang-inets sqlite sqlite3 libsqlite3-dev && \
    git clone --branch v1.6.6 https://github.com/elixir-lang/elixir.git && \
    pushd elixir && \
    make -j16 install && \
    ln -s /usr/local/bin/mix /usr/bin/mix && \
    popd && \
    git clone --branch 0.9.14 https://github.com/pprzetacznik/IElixir.git && \
    pushd IElixir && \
    yes | mix local.hex && \
    yes | mix local.rebar --force && \
    mix deps.get && \
    mix deps.compile erlzmq && \
    mix deps.compile esqlite && \
    MIX_ENV=prod mix compile && \
    ./install_script.sh && \
    popd

### Erlang and
### LFE
RUN cd /opt && \
    git clone https://github.com/filmor/ierl.git && \
    pushd /opt/ierl && \
    ./_download_rebar3.sh && \
    ./rebar3 escriptize && \
    pushd /opt/ierl/_build/default/bin && \
        ./ierl install erlang && \
        ./ierl install lfe && \
        sed -i "s/ (erlang)//g" /usr/local/share/jupyter/kernels/erlang/kernel.json && \
        sed -i "s/ (lfe)//g" /usr/local/share/jupyter/kernels/lfe/kernel.json && \
    popd && popd
COPY ./icons/erlang/ /usr/local/share/jupyter/kernels/erlang/
COPY ./icons/lfe/ /usr/local/share/jupyter/kernels/lfe/

### Elm
RUN cd /opt && \
    npm install --global elm && \
    git clone https://github.com/abingham/jupyter-elm-kernel.git && \
    pushd jupyter-elm-kernel && \
    pip install -e . && \
    python -m elm_kernel.install && \
    popd
COPY ./icons/elm/ /usr/local/share/jupyter/kernels/elm/

### Xeus-cling
### C++
RUN cd /opt && \
    conda create -n cling -y -c conda-forge xeus-cling xtensor xtensor-blas && \
    jupyter kernelspec install /opt/conda/envs/cling/share/jupyter/kernels/xcpp17

### bash
RUN cd /opt && \
    conda install -y -c conda-forge bash_kernel && \
    python -m bash_kernel.install
COPY ./icons/bash/ /opt/conda/share/jupyter/kernels/bash/

### zsh
RUN cd /opt && \
    apt-get install -y zsh figlet && \
    git clone https://github.com/danylo-dubinin/zsh-jupyter-kernel.git && \
    pushd /opt/zsh-jupyter-kernel/src && \
    wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh && \
    sh install.sh --unattended && \
    git checkout tags/3.2 && \
    echo "from distutils.core import setup" > ./setup.py && \
    echo "setup(name='zsh_jupyter_kernel', version='$(cat zsh_jupyter_kernel/version)', packages=['zsh_jupyter_kernel'])" >> ./setup.py && \
    pip install -e . && \
    pushd /opt/zsh-jupyter-kernel/src/zsh_jupyter_kernel && \
        python -m zsh_jupyter_kernel.install --sys-prefix && \
    popd && popd
COPY ./icons/zsh/ /opt/conda/share/jupyter/kernels/zsh/

### vimscript
RUN cd /opt && \
    git clone https://github.com/mattn/vim_kernel.git && \
    pushd vim_kernel && \
    python setup.py install && \
    python -m vim_kernel.install && \
    popd

### C
RUN cd /opt && \
    pip install jupyter-c-kernel && \
    install_c_kernel
COPY ./icons/c/ /root/.local/share/jupyter/kernels/c/

### GO
ENV PATH /opt/go/bin:$PATH
RUN cd /opt && \
    wget https://dl.google.com/go/go1.14.2.linux-amd64.tar.gz && \
    tar xvf go1.14.2.linux-amd64.tar.gz && \
    export PATH=$PATH:/opt/go/bin && \
    env GO111MODULE=on go get github.com/gopherdata/gophernotes && \
    mkdir -p /opt/conda/share/jupyter/kernels/gophernotes && \
    pushd /opt/conda/share/jupyter/kernels/gophernotes && \
    cp "$(go env GOPATH)"/pkg/mod/github.com/gopherdata/gophernotes@v0.7.0/kernel/*  "." && \
    chmod +w ./kernel.json && \
    sed "s|gophernotes|$(go env GOPATH)/bin/gophernotes|" < kernel.json.in > kernel.json && \
    export GOPATH=$(go env GOPATH) && \
    echo "GOPATH=$GOPATH" >> /etc/bash.bashrc && \
    go get fmt math gonum.org/v1/gonum/mat time flag io/ioutil log os path/filepath strings && \
    go get unicode encoding/json github.com/machinebox/sdk-go/facebox && \
    popd

### Hy
RUN cd /opt && \
    pip install ipyparallel && \
    pip install git+https://github.com/hylang/hy.git@0.17.0 && \
    pip install git+https://github.com/ekaschalk/jedhy.git && \
    pip install git+https://github.com/Karrq/calysto_hy.git && \
    python -m calysto_hy install && \
    sed -i "s/Calysto Hy/Hy/g" /usr/local/share/jupyter/kernels/calysto_hy/kernel.json
COPY ./icons/hy/ /usr/local/share/jupyter/kernels/calysto_hy/

### MIT Scheme
RUN cd /opt && \
    wget https://ftp.gnu.org/gnu/mit-scheme/stable.pkg/10.1.10/mit-scheme-10.1.10-x86-64.tar.gz && \
    tar xvf mit-scheme-10.1.10-x86-64.tar.gz && \
    wget https://github.com/zeromq/libzmq/releases/download/v4.3.1/zeromq-4.3.1.tar.gz && \
    tar xvf zeromq-4.3.1.tar.gz && \
    git clone https://github.com/joeltg/mit-scheme-kernel && \
    pushd /opt/mit-scheme-10.1.10/src && \
    ./configure --prefix=/usr/local && make -j16 && make -j16 install && \
    popd && pushd /opt/zeromq-4.3.1 && \
    ./configure --prefix=/usr/local && make -j16 && make -j16 install && \
    popd && pushd /opt/mit-scheme-kernel && \
    make -j16 && make -j16 install && \
    popd

### Calysto Scheme
RUN cd /opt && \
    pip install yasi==2.1.0 && \
    pip install --upgrade calysto-scheme && \
    python -m calysto_scheme install

### PHP
RUN cd /opt && \
    apt-get install -y php php-zmq && \
    mkdir jupyter-php && \
    pushd jupyter-php && \
    wget https://litipk.github.io/Jupyter-PHP-Installer/dist/jupyter-php-installer.phar && \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    chmod +x ./jupyter-php-installer.phar && \
    ./jupyter-php-installer.phar install && \
    popd
COPY ./icons/php/ /usr/local/share/jupyter/kernels/jupyter-php/

### Raku
### Perl 6
ENV PATH /usr/local/share/perl6/site/bin:$PATH
RUN cd /opt && \
    apt-get install -y perl && \
    wget https://rakudo.org/downloads/star/rakudo-star-2020.01.tar.gz && \
    tar xvf rakudo-star-2020.01.tar.gz && \
    pushd rakudo-star-2020.01 && \
    perl Configure.pl --gen-moar --make-install --prefix /usr/local && \
    popd && \
    git clone https://github.com/ugexe/zef.git && \
    pushd zef && \
    perl6 -I. bin/zef install . && \
    zef install Jupyter::Kernel && \
    jupyter-kernel.raku --generate-config && \
    popd && \
    wget -O - http://cpanmin.us | perl - --self-upgrade && \
    cpanm install XML::DOM XML::Parser

### J
ENV J_INSTALLATION_FOLDER /opt/j901
ENV J_BIN_FOLDER /opt/j901/bin
RUN  cd /opt && \
    wget http://www.jsoftware.com/download/j901/install/j901_linux64.tar.gz && \
    tar xvf j901_linux64.tar.gz && \
    mkdir -p /opt/j901/user/temp && \
    echo "home=.	    install,'/user'" >	/opt/j901/bin/profilex.ijs && \
    echo "user=.	    install,'/user'" >> /opt/j901/bin/profilex.ijs && \
    echo "userx=.	    install,'/user'" >> /opt/j901/bin/profilex.ijs && \
    echo "" >>					/opt/j901/bin/profilex.ijs && \
    echo "break=.	    user,'/break'" >>	/opt/j901/bin/profilex.ijs && \
    echo "config=.	    user,'/config'" >>	/opt/j901/bin/profilex.ijs && \
    echo "snap=.	    user,'/snap'" >>	/opt/j901/bin/profilex.ijs && \
    echo "temp=.	    user,'/temp'" >>	/opt/j901/bin/profilex.ijs && \
    echo "install('all')" | /opt/j901/bin/jconsole && \
    git clone https://github.com/martin-saurer/jkernel.git && \
    pushd jkernel && \
    python setup.py install && \
    mv /opt/jkernel/Jupyter_Notebook_J_Example_Data /opt/j901/user/ && \
    popd

### LC3
RUN cd /opt && \
    pip install calysto_lc3 && \
    python -m calysto_lc3.install
COPY ./icons/lc3/ /root/.local/share/jupyter/kernels/calysto_lc3/
    
### Ada
RUN cd /opt && \
    apt-get install -y gnat && \
    git clone https://github.com/gusthoff/jupyter-ada-kernel.git && \
    pushd jupyter-ada-kernel && \
    pip install -e . && \
    pushd jupyter_ada_kernel && \
        conda run python install_ada_kernel && \
    popd && popd
COPY ./icons/ada/ /root/.local/share/jupyter/kernels/ada/

### TCL
RUN cd /opt && \
    apt-get install -y tcl tcl-dev liblasi0 liblasi-dev && \
    pip install tcl_kernel --user && \
    conda run python -m tcl_kernel.install
COPY ./icons/tcl/ /usr/local/share/jupyter/kernels/tcl/

### GDL (IDL clone) -- very complex, not working great for image output
RUN cd /opt && \
    apt-get install -y swig camlidl wx-common \
    gsl-bin libgsl23 libgsl-dev \
    libgslcblas0 imagemagick graphicsmagick \
    graphicsmagick-libmagick-dev-compat \
    libgeotiff-dev libgeotiff2 \
    libnetcdf-dev libhdf4-alt-dev libeccodes-dev \
    libshp-dev libudunits2-dev libeigen3-dev pslib-dev \
    libproj-dev proj-bin && \
    pip install cython && \
    git clone --branch plplot-5.15.0 https://git.code.sf.net/p/plplot/plplot && \
    mkdir /opt/plplot/build && \
    pushd /opt/plplot/build && \
    # yes for some reason this really does need to be run twice
    cmake -DENABLE_qt=OFF \
        -DENABLE_pyqt5=OFF \
        -DENABLE_d=OFF \
        -DENABLE_wxwidgets=OFF \
        -DENABLE_tk=OFF \
        -DENABLE_itk=OFF \
        -DENABLE_DYNDRIVERS=OFF \
        .. && \
    cmake -DENABLE_qt=OFF \
        -DENABLE_pyqt5=OFF \
        -DENABLE_d=OFF \
        -DENABLE_wxwidgets=OFF \
        -DENABLE_tk=OFF \
        -DENABLE_itk=OFF \
        -DENABLE_DYNDRIVERS=OFF \
        -DCMAKE_CXX_COMPILER=/usr/bin/gcc \
        -DCMAKE_C_COMPILER=/usr/bin/cc \
        .. && \
    make -j16 && make -j16 install && \
    popd && \
    git clone --branch v1.0.0-rc.2 https://github.com/gnudatalanguage/gdl.git && \
    mkdir -p /opt/gdl/build && \
    wget https://www.ece.uvic.ca/~frodo/jasper/software/jasper-2.0.14.tar.gz && \
    tar xvf jasper-2.0.14.tar.gz && \
    mkdir /opt/jasper-2.0.14/build_dir && pushd /opt/jasper-2.0.14/build_dir && \
    cmake .. && \
    make -j16 && make -j16 install && \
    popd && \
    git clone --branch v1.18.5 https://github.com/numpy/numpy.git && \
    pushd /opt/numpy && \
    python setup.py build_ext --inplace -j 16 && \
    popd && \
    pushd /opt/gdl/build && \
    cmake -DPYTHON=ON  \
        -DPYTHONVERSION=3.7 \
        -DWXWIDGETS=OFF \
        -DCMAKE_CXX_FLAGS="-luuid" \
        -DCMAKE_CXX_COMPILER=/usr/bin/g++ \
        -DCMAKE_C_COMPILER=/usr/bin/gcc \
        -DGSLDIR=/usr/lib/x86_64-linux-gnu  \
        -DPLPLOTDIR=/usr/local/lib  \
        -DPLPLOT_INCLUDE_DIR=/usr/local/include/plplot \
        -DGRAPHICSMAGICKDIR=/usr/lib \
        -DMAGICKDIR=/usr/lib \
        -DMAGICK=OFF \
        -DPYTHON_NUMPY_INCLUDE_DIR=/opt/numpy/numpy/core/include \
        .. && \
    make -j16 && make -j16 install && \
    conda install -y pexpect && \
    popd && \
    git clone https://github.com/birnam/gdl_kernel.git && \
    pushd /opt/gdl_kernel && \
    python setup.py install --prefix=/opt/conda && \
    popd
COPY ./icons/gdl/ /root/.local/share/jupyter/kernels/GDL/

### Singular
RUN cd /opt && \
    apt-get install -y yasm graphviz && \
    mkdir /opt/singular && pushd /opt/singular && \
      git clone git://github.com/wbhart/mpir.git && \
      pushd mpir && \
        ./autogen.sh && ./configure && make -j16 && make -j16 install && \
      popd && \
      git clone https://github.com/wbhart/flint2.git && \
      pushd flint2 && \
        export CC=/usr/bin/gcc && export CXX=/usr/bin/g++ && \
        ./configure --with-gmp=/usr --prefix=/usr/local --with-mpfr=/usr \
            --with-mpir=/usr/local && \
        make -j16 && make -j16 install && \
    popd && popd && \
    pushd /opt/singular && \
      git clone https://github.com/4ti2/4ti2.git && \
      export CC=/usr/bin/gcc && export CXX=/usr/bin/g++ && \
      pushd 4ti2 && \
        ./autogen.sh && ./configure --with-glpk=/usr && \
        make -j16 && make -j16 install && make -j16 install-exec && \
      popd && \
      git clone -b Release-4-1-3p2 https://github.com/Singular/Sources.git singular && \
      pushd singular && \
        ./autogen.sh && \
        ./configure --with-flint=/usr/local --enable-gfanlib --prefix=/usr/local && \
        make -j16 && make -j16 install && \
      popd && \
      apt-get install -y gtkmm-2.4 libtiff5-dev flex surf-alggeo && \
      ln -s /usr/bin/surf-alggeo /usr/bin/surf && \
      pip install PySingular && \
      git clone https://github.com/birnam/jupyter_kernel_singular.git && \
      pushd jupyter_kernel_singular && \
        conda run python setup.py install && \
  popd && popd

### Pike
RUN cd /opt && \
    apt-get install -y pike8.0 && \
    pip install pike_kernel && \
    conda run python -m pike_kernel.install
COPY ./icons/pike/ /usr/local/share/jupyter/kernels/pike/

### XonSH
RUN cd /opt && \
    conda install -y -c conda-forge xonsh
COPY ./icons/xonsh/ /opt/conda/share/jupyter/kernels/xonsh/

### Coconut
RUN cd /opt && \
    pip install coconut[all] && \
    coconut --jupyter && \
    yes | jupyter kernelspec remove coconut2 coconut3
COPY ./icons/coconut/ /usr/local/share/jupyter/kernels/coconut/

### Racket
RUN cd /opt && \
    wget https://mirror.racket-lang.org/installers/7.7/racket-7.7-x86_64-linux.sh && \
    chmod a+x racket-7.7-x86_64-linux.sh && \
    ./racket-7.7-x86_64-linux.sh --unix-style --dest /usr && \
    raco pkg install --auto iracket && \
    raco iracket install

### Dot / Graphviz
RUN cd /opt && \
    apt-get install -y graphviz && \
    pip install dot_kernel && \
    install-dot-kernel
COPY ./icons/dot/ /root/.local/share/jupyter/kernels/dot_kernel_spec/

### Gnuplot
RUN cd /opt && \
    pip install gnuplot_kernel && \
    conda run python -m gnuplot_kernel install --user && \
    conda install -y matplotlib

### Chapel
ENV CHPL_LIB_PIC=shared
RUN cd /opt && \
    wget https://github.com/chapel-lang/chapel/releases/download/1.22.0/chapel-1.22.0.tar.gz && \
    tar xvf chapel-1.22.0.tar.gz && \
    export CHPL_LIB_PIC=shared && \
    pushd chapel-1.22.0 && \
    echo "source /opt/chapel-1.22.0/util/setchplenv.bash > /dev/null" >> /etc/bash.bashrc && \
    source /opt/chapel-1.22.0/util/setchplenv.bash && \
    make -j16 && \
    pip install jupyter-kernel-chapel && \
    popd
COPY ./icons/chapel/ /opt/conda/share/jupyter/kernels/chapel/

### GAP
ENV JUPYTER_GAP_EXECUTABLE /opt/gap-4.11.0/bin/x86_64-pc-linux-gnu-default64-kv7/gap
ENV PATH $PATH:/opt/gap-4.11.0/bin/x86_64-pc-linux-gnu-default64-kv7:/opt/gap-4.11.0/JupyterKernel-1.3/bin
RUN cd /opt && \
    apt-get install -y libcurl4-openssl-dev libnormaliz-dev && \
    export CC=/usr/bin/gcc && \
    export CXX=/usr/bin/g++ && \
    wget https://files.gap-system.org/gap-4.11/tar.bz2/gap-4.11.0.tar.bz2 && \
    tar xvf gap-4.11.0.tar.bz2 && \
    pushd gap-4.11.0 && \
    ./configure && make -j16 && \
    pushd pkg && \
        ../bin/BuildPackages.sh && \
    popd && \
    wget https://github.com/gap-packages/JupyterKernel/releases/download/v1.3/JupyterKernel-1.3.tar.gz && \
    tar xvf JupyterKernel-1.3.tar.gz && \
    pushd JupyterKernel-1.3 && \
       conda run python setup.py install && \
    popd && \
    popd

### PARI/GP
RUN cd /opt && \
    apt-get install -y pari-gp && \
    conda install -y -c conda-forge pari_jupyter

### CoffeeScript
RUN cd /opt && \
    npm install -g jp-coffeescript && \
    jp-coffee-install
COPY ./icons/coffeescript/ /root/.local/share/jupyter/kernels/coffeescript/

### LiveScript
RUN cd /opt && \
    git clone https://github.com/birnam/jp-livescript.git && \
    pushd jp-livescript && \
    npm install -g  && \
    jp-livescript-install && \
    popd
COPY ./icons/livescript/ /root/.local/share/jupyter/kernels/livescript/

### GrADS
ENV SUPPLIBS /opt/grads/supplibs
ENV PATH $PATH:/opt/grads/supplibs/bin
ENV GADDIR /usr/local/lib/grads
ENV GAUDPT /opt/grads/udpt
RUN cd /opt && \
    export CFLAGS=" -fPIC" && \
    export SUPPLIBS=/opt/grads/supplibs && \
    ln -s /bin/sed /usr/bin/sed && \
    echo "deb-src http://deb.debian.org/debian buster main" >> /etc/apt/sources.list && \
    echo "deb-src http://security.debian.org/debian-security buster/updates main" >> /etc/apt/sources.list && \
    echo "deb-src http://deb.debian.org/debian buster-updates main" >> /etc/apt/sources.list && \
    apt-get update --allow-releaseinfo-change && \
    apt-get install -y meson bison gperf libxmu-dev xutils-dev \
    freeglut3 freeglut3-dev doxygen doxygen-latex libssh2-1 libssh2-1-dev \
    libssl-dev libjpeg62-turbo libjpeg62-turbo-dev && \
    mkdir -p /opt/grads/supplibs/src && \
    mkdir -p /opt/grads/supplibs/tarfiles && \
    mkdir -p /opt/grads/supplibs/man/man1 && \
    pushd /opt/grads/supplibs/tarfiles && \
    wget https://github.com/libgd/libgd/releases/download/gd-2.3.0/libgd-2.3.0.tar.gz && \
    wget ftp://cola.gmu.edu/grads/Supplibs/2.2/src/libpng-1.5.12.tar.gz && \
    wget https://versaweb.dl.sourceforge.net/project/libpng/libpng16/1.6.37/libpng-1.6.37.tar.gz && \
    wget ftp://cola.gmu.edu/grads/Supplibs/2.2/src/tiff-3.8.2.tar.gz && \
    wget https://zlib.net/fossils/zlib-1.2.9.tar.gz && \
    wget https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.10/hdf5-1.10.6/src/CMake-hdf5-1.10.6.tar.gz && \
    wget https://support.hdfgroup.org/ftp/lib-external/szip/2.1.1/src/szip-2.1.1.tar.gz && \
    wget https://curl.haxx.se/download/curl-7.66.0.tar.gz && \
    wget https://download.osgeo.org/proj/proj-7.0.1.tar.gz && \
    wget ftp://cola.gmu.edu/grads/Supplibs/2.2/src/libgeotiff-1.2.5.tar.gz && \
    wget ftp://cola.gmu.edu/grads/Supplibs/2.2/src/cairo-1.14.10.tar.gz && \
    wget https://pkg-config.freedesktop.org/releases/pkg-config-0.29.2.tar.gz && \
    wget https://download.gnome.org/sources/glib/2.64/glib-2.64.3.tar.xz && \
    wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-c-4.7.4.tar.gz && \
    wget https://www.nco.ncep.noaa.gov/pmb/codes/GRIB2/g2clib-1.6.0.tar && \
    wget ftp://cola.gmu.edu/grads/Supplibs/2.2/src/libxml2-2.9.0.tar.gz && \
    wget ftp://ftp.unidata.ucar.edu/pub/udunits/udunits-1.12.11.tar.gz && \
    wget ftp://ftp.pcre.org/pub/pcre/pcre-8.44.tar.gz && \
    wget ftp://cola.gmu.edu/grads/data2.tar.gz && \
    popd && \
    pushd /opt/grads/supplibs/src && \
    git clone https://github.com/mdadams/jasper.git && \
    pushd jasper && \
        mkdir build-dir && pushd build-dir && \
        cmake -DCMAKE_INSTALL_PREFIX=$SUPPLIBS -DJAS_ENABLE_OPENGL=false -DOPENGL_GL_PREFERENCE=LEGACY \
            -DJAS_ENABLE_SHARED=true -DJAS_ENABLE_AUTOMATIC_DEPENDENCIES=false .. && \
        make -j16 install && \
        popd && \
    popd && \
    tar xvfz ../tarfiles/libpng-1.5.12.tar.gz && \
    pushd libpng-1.5.12 && \
        ./configure --prefix=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    tar xvfz ../tarfiles/tiff-3.8.2.tar.gz && \
    pushd tiff-3.8.2 && \
        ./autogen.sh && \
        ./configure --enable-shared --prefix=$SUPPLIBS \
        --with-jpeg-include-dir=$SUPPLIBS/include \
        --with-jpeg-lib-dir=$SUPPLIBS/lib && \
        make -j16 install LIBTOOL=/usr/bin/libtool && \
    popd && \
    tar xvfz ../tarfiles/szip-2.1.1.tar.gz && \
    pushd szip-2.1.1 && \
        mkdir build && pushd build && \
        cmake -DCMAKE_INSTALL_PREFIX=$SUPPLIBS .. && \
        make -j16 install && \
        popd && \
    popd && \
    tar xvfz ../tarfiles/zlib-1.2.9.tar.gz && \
    pushd zlib-1.2.9 && \
        mkdir build && pushd build && \
        cmake -DCMAKE_INSTALL_PREFIX=$SUPPLIBS -DLIBRARY_OUTPUT_PATH:PATH=$SUPPLIBS/lib .. && \
        make -j16 install && \
        popd && \
    popd && \
    tar xvfz ../tarfiles/proj-7.0.1.tar.gz && \
    pushd proj-7.0.1 && \
        mkdir build && pushd build && \
        cmake -DCMAKE_INSTALL_PREFIX=$SUPPLIBS .. && \
        make -j16 install && \
        popd && \
    popd && \
    git clone https://github.com/OSGeo/libgeotiff.git && \
    pushd libgeotiff/libgeotiff && \
        git checkout 1.6.0 && \
        ./autogen.sh && \
        ./configure --prefix=$SUPPLIBS --with-zlib=$SUPPLIBS --with-jpeg \
        --with-proj=$SUPPLIBS --with-libtiff=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    tar xvfz ../tarfiles/curl-7.66.0.tar.gz && \
    pushd curl-7.66.0 && \
        mkdir build && pushd build && \
        cmake -DCMAKE_INSTALL_PREFIX=$SUPPLIBS \
            -DLIBSSH2_INCLUDE_DIR=/usr/include \
            -DLIBSSH2_LIBRARY=/usr/lib/x86_64-linux-gnu/libssh2.so \
            -DZLIB_LIBRARY_RELEASE:FILEPATH=$SUPPLIBS/lib/libz.so \
            -DZLIB_INCLUDE_DIR:PATH=$SUPPLIBS/include .. && \
        make -j16 install && \
        popd && \
    popd && \
    tar xvfz ../tarfiles/libxml2-2.9.0.tar.gz && \
    pushd libxml2-2.9.0 && \
        LDFLAGS="-L$SUPPLIBS/lib" CPPFLAGS="-I$SUPPLIBS/include" \
        ./configure --prefix=$SUPPLIBS --with-zlib=$SUPPLIBS --without-threads \
        --without-iconv --without-iso8859x --without-lzma && \
        make -j16 install && \
    popd && \
    tar xvf ../tarfiles/g2clib-1.6.0.tar && \
    pushd g2clib-1.6.0 && \
        sed -i \
        -e "s|^INC *= *.*|INC=-I$SUPPLIBS/include -I$SUPPLIBS/include/libpng15|" \
        -e "s|^CFLAGS *= *\(.*\)|CFLAGS=\1 -fPIC|" \
        makefile && \
        sed -i \
        -e "s|\(.*\)\(image\.inmem_=1;\)|\1//\2|" \
        enc_jpeg2000.c && \
        sed -i \
        -e "s|\(.*\)\(image->inmem_.*\)|//\1\2|" \
        dec_jpeg2000.c && \
        PKG_CONFIG=$SUPPLIBS/bin/pkg-config \
        PKG_CONFIG_PATH=$SUPPLIBS/lib/pkgconfig \
        make -j16 && \
        cp -f libg2c_v1.6.0.a $SUPPLIBS/lib/libgrib2c.a && \
        cp -f grib2.h $SUPPLIBS/include/grib2.h && \
    popd && \
    git clone https://bitbucket.hdfgroup.org/scm/hdffr/hdf4.git && \
    pushd hdf4 && \
        git checkout hdf-4_2_15 && \
        ./configure --prefix=$SUPPLIBS --enable-shared \
        --with-szlib=$SUPPLIBS --with-zlib=$SUPPLIBS \
        --disable-fortran && \
        make -j16 install && \
    popd && \
    tar xvfz ../tarfiles/CMake-hdf5-1.10.6.tar.gz && \
    pushd CMake-hdf5-1.10.6 && \
        ctest -S HDF5config.cmake,BUILD_GENERATOR=Unix,INSTALLDIR=$SUPPLIBS -C Release -VV -O hdf5.log && \
        ./HDF5-1.10.6-Linux.sh --skip-license --prefix=$SUPPLIBS && \
        ln -sf $SUPPLIBS/HDF_Group/HDF5/1.10.6/bin/* $SUPPLIBS/bin/ && \
        ln -sf $SUPPLIBS/HDF_Group/HDF5/1.10.6/lib/lib* $SUPPLIBS/lib/ && \
        ln -sf $SUPPLIBS/HDF_Group/HDF5/1.10.6/include/* $SUPPLIBS/include/ && \
        ln -sf $SUPPLIBS/HDF_Group/HDF5/1.10.6/share/cmake/* $SUPPLIBS/share/cmake/ && \
    popd && \
    git clone git://git.sv.nongnu.org/freetype/freetype2.git && \
    pushd freetype2 && \
        ./autogen.sh && \
        ./configure --prefix=$SUPPLIBS --enable-shared && \
        make -j16 install && \
    popd && \
    tar xvfz ../tarfiles/libpng-1.6.37.tar.gz && \
    pushd libpng-1.6.37 && \
        LDFLAGS="-L$SUPPLIBS/lib" CPPFLAGS="-I$SUPPLIBS/lib" \
        ./configure --prefix=$SUPPLIBS --with-zlib-prefix=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    git clone https://gitlab.freedesktop.org/fontconfig/fontconfig.git && \
    pushd fontconfig && \
        ./autogen.sh && \
        LDFLAGS="-L$SUPPLIBS/lib" CPPFLAGS="-I$SUPPLIBS/lib" \
        ./configure --prefix=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    tar xvfz ../tarfiles/libgd-2.3.0.tar.gz && \
    pushd libgd-2.3.0 && \
        ./configure --prefix=$SUPPLIBS --with-jpeg --with-fontconfig=$SUPPLIBS --with-png=$SUPPLIBS \
        --with-tiff=$SUPPLIBS --with-zlib=$SUPPLIBS --with-freetype=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    tar xvfz ../tarfiles/pkg-config-0.29.2.tar.gz && \
    pushd pkg-config-0.29.2 && \
        ./configure --prefix=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    tar xvfJ ../tarfiles/glib-2.64.3.tar.xz && \
    pushd glib-2.64.3 && \
        meson _build --prefix=$SUPPLIBS && \
        ninja -C _build && \
        ninja -C _build install && \
        cp $SUPPLIBS/lib/x86_64-linux-gnu/glib-2.0/include/* $SUPPLIBS/include/ && \
        cp $SUPPLIBS/lib/x86_64-linux-gnu/lib* $SUPPLIBS/lib/ && \
        cp -r $SUPPLIBS/lib/x86_64-linux-gnu/pkgconfig/* $SUPPLIBS/lib/pkgconfig/ && \
    popd && \
    git clone https://github.com/freedesktop/pixman.git && \
    pushd pixman && \
        ./autogen.sh && \
        ./configure --prefix=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    git clone https://gitlab.freedesktop.org/xorg/lib/libx11.git && \
    pushd libx11 && \
        ./autogen.sh && \
        ./configure --prefix=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    git clone https://gitlab.freedesktop.org/xorg/proto/xorgproto.git && \
    pushd xorgproto && \
        ./autogen.sh && \
        ./configure --prefix=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    git clone git://anongit.freedesktop.org/git/cairo && \
    pushd cairo && \
        ./autogen.sh && \
        ./configure --prefix=$SUPPLIBS --enable-xlib=yes --enable-xml=yes --enable-fc=yes \
        --enable-ft=yes --enable-xlib-xrender=yes --enable-pthread=yes \
        --enable-xcb=no --enable-qt=no --enable-quartz=no --enable-win32=no \
        --enable-os2=no --enable-beos=no --enable-drm=no --enable-gl=no --enable-gobject=yes && \
        make -j16 install && \
    popd && \
    git clone https://github.com/harfbuzz/harfbuzz.git && \
    pushd harfbuzz && \
        ./autogen.sh && \
        ./configure --prefix=$SUPPLIBS --with-glib=yes --with-cairo=yes \
        --with-fontconfig=yes --with-freetype=yes && \
        make -j16 install && \
    popd && \
    git clone https://github.com/libexpat/libexpat.git && \
    pushd libexpat/expat && \
        git checkout R_2_2_9 && \
        mkdir build && pushd build && \
        cmake -DCMAKE_INSTALL_PREFIX=$SUPPLIBS .. && \
        make -j16 install && \
        popd && \
    popd && \
    popd && \
    pushd /opt/grads/supplibs/src && \
    tar xvfz ../tarfiles/udunits-1.12.11.tar.gz && \
    pushd udunits-1.12.11/src && \
        CFLAGS="-fPIC" LDFLAGS="-L$SUPPLIBS/lib" \
        CPPFLAGS="-Df2cFortran -I$SUPPLIBS/include" \
        PERL=/usr/bin/perl \
        ./configure --prefix=$SUPPLIBS && \
        make && \
        make install && \
    popd && \
    export PKG_CONFIG=$SUPPLIBS/bin/pkg-config && \
    export PKG_CONFIG_PATH=$SUPPLIBS/lib/pkgconfig && \
    tar xvfz ../tarfiles/netcdf-c-4.7.4.tar.gz && \
    pushd netcdf-c-4.7.4 && \
        mkdir build && pushd build && \
        cmake -DCMAKE_INSTALL_PREFIX:PATH=$SUPPLIBS -DENABLE_HDF4=ON \
          -DENABLE_NETCDF4:BOOL=ON -DENABLE_NETCDF_4:BOOL=ON -DUSE_NETCDF4:BOOL=ON \
          -DZLIB_INCLUDE_DIR:PATH=$SUPPLIBS/include \
          -DZLIB_LIBRARY:FILEPATH=$SUPPLIBS/lib/libz.so \
          -DHAVE_HDF5_H:PATH=$SUPPLIBS/include \
          -DHDF5_DIR:PATH=$SUPPLIBS/share/cmake/hdf5 \
          -DUSE_SZIP:BOOL=ON -DSZIP:FILEPATH=$SUPPLIBS/lib/libszip.so \
          -DCURL_LIBRARY_RELEASE:FILEPATH=$SUPPLIBS/lib/libcurl.so \
          -DCURL_INCLUDE_DIR:PATH=$SUPPLIBS/include/curl \
          .. && \
        make -j16 install && \
        popd && \
    popd && \
    popd && \
    export PKG_CONFIG=$SUPPLIBS/bin/pkg-config && \
    export PKG_CONFIG_PATH=$SUPPLIBS/lib/pkgconfig && \
    pushd /opt/grads/supplibs/src && \
    git clone https://github.com/freedesktop/xorg-macros.git && \
    pushd xorg-macros && \
        ./autogen.sh && \
        ./configure --prefix=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    git clone https://gitlab.freedesktop.org/xorg/lib/libxau.git && \
    pushd libxau && \
        ./autogen.sh && \
        ./configure --prefix=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    git clone https://gitlab.freedesktop.org/xorg/proto/xcbproto.git && \
    pushd xcbproto && \
        ./autogen.sh && \
        ./configure --prefix=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    git clone https://gitlab.freedesktop.org/xorg/lib/libxcb.git && \
    pushd libxcb && \
        ./autogen.sh && \
        ./configure --prefix=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    git clone https://github.com/freedesktop/xorg-libXrender.git && \
    pushd xorg-libXrender && \
        ./autogen.sh && \
        ./configure --prefix=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    git clone https://gitlab.freedesktop.org/xorg/lib/libxext.git && \
    pushd libxext && \
        ./autogen.sh && \
        ./configure --prefix=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    tar xvfz ../tarfiles/pcre-8.44.tar.gz && \
    pushd pcre-8.44 && \
        ./configure --prefix=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    git clone https://github.com/libffi/libffi.git && \
    pushd libffi && \
        ./autogen.sh && \
        ./configure --prefix=$SUPPLIBS && \
        make -j16 install && \
    popd && \
    popd && \
    export LD_RUN_PATH=$SUPPLIBS/lib && \
    export CXX=/usr/bin/g++ && \
    export CC=/usr/bin/gcc && \
    pushd /opt/grads && \
    wget ftp://cola.gmu.edu/grads/2.2/grads-2.2.1-src.tar.gz && \
    tar xvf grads-2.2.1-src.tar.gz && \
    pushd $SUPPLIBS/src/g2clib-1.6.0 && \
        # unknown why this needs a second go
        make && \
        cp -f libg2c_v1.6.0.a $SUPPLIBS/lib/libgrib2c.a && \
        cp -f grib2.h $SUPPLIBS/include/grib2.h && \
    popd && pushd grads-2.2.1 && \
        ./configure --prefix=/usr --without-gadap && \
        make -j16 install && \
    popd && \
    popd && \
    mkdir -p /opt/grads/supplibs/grads && \
    pushd /opt/grads/supplibs/grads && \
    tar xvfz ../tarfiles/data2.tar.gz && \
    popd && \
    mv /opt/grads/supplibs/grads /usr/local/lib/grads && \
    rm -f /opt/grads/udpt && \
    echo "gxdisplay gxdummy /usr/lib/libgxdummy.so" >> /opt/grads/udpt && \
    echo "gxdisplay Cairo /usr/lib/libgxdCairo.so" >> /opt/grads/udpt && \
    echo "gxdisplay X11 /usr/lib/libgxdX11.so" >> /opt/grads/udpt && \
    echo "gxprint gxdummy /usr/lib/libgxdummy.so" >> /opt/grads/udpt && \
    echo "gxprint Cairo /usr/lib/libgxpCairo.so" >> /opt/grads/udpt && \
    echo "gxprint GD /usr/lib/libgxpGD.so" >> /opt/grads/udpt && \
    pushd /opt/grads && \
    git clone https://github.com/ykatsu111/jupyter-grads-kernel.git && \
    pushd jupyter-grads-kernel && \
        pip install --user -e . && \
        jupyter kernelspec install --user grads_spec && \
    popd && \
    popd
COPY ./icons/grads/ /root/.local/share/jupyter/kernels/grads_spec/

### Prolog
RUN cd /opt && \
    pip install --upgrade calysto_prolog && \
    conda run python -m calysto_prolog install
COPY ./icons/swiprolog/ /usr/local/share/jupyter/kernels/calysto_prolog/

### Root
ENV PYTHONPATH $PYTHONPATH:/opt/conda/lib/python3.7/site-packages:/usr/local/lib
RUN cd /opt && \
    apt-get install libxpm-dev --fix-missing && \
    wget https://root.cern/download/root_v6.20.04.source.tar.gz && \
    tar xvfz root_v6.20.04.source.tar.gz && \
    pushd root-6.20.04 && \
    mkdir builddir && pushd builddir && \
        cmake -DCMAKE_INSTALL_PYROOTDIR:PATH=/opt/conda/lib/python3.7/site-packages \
        -Dpython3:BOOL=ON \
        .. && \
        cmake --build . && \
        make -j16 install && \
        rm -rf /usr/local/etc/notebook/kernels/root && \
        cp -r ./etc/notebook/kernels/root /usr/local/share/jupyter/kernels/root && \
    popd && \
    popd && \
    echo ". /usr/local/bin/thisroot.sh" >> /etc/bash.bashrc

### Jython
ENV JAVA_HOME /opt/conda
ENV JYTHON_HOME /opt/jython
ENV PATH $PATH:/opt/jython/bin
RUN cd /opt && \
    wget https://cfhcable.dl.sourceforge.net/project/jython/jython/2.5.2/jython_installer-2.5.2.jar && \
    java -jar jython_installer-2.5.2.jar --silent --directory /opt/jython --type all && \
    git clone https://github.com/birnam/IJython.git && \
    pushd IJython && \
    conda run python setup.py install && \
    popd
COPY ./icons/jython/ /usr/local/share/jupyter/kernels/jython_kernel/

### Arm
RUN cd /opt && \
    git clone https://github.com/DeepHorizons/iarm.git && \
    pushd iarm && \
    conda run python setup.py install && \
    conda run python -m iarm_kernel.install && \
    popd
COPY ./icons/arm/ /usr/local/share/jupyter/kernels/iarm/

### Mochi
RUN cd /opt && \
    git clone https://github.com/birnam/mochi.git && \
    pushd mochi && \
    conda run python setup.py install && \
    pip install flask Flask-RESTful Pillow RxPY boto boto3 && \
    popd && \
    git clone https://github.com/pya/mochi-kernel.git && \
    pushd mochi-kernel && \
    conda run python setup.py install && \
    popd
COPY ./icons/mochi/ /root/.local/share/jupyter/kernels/mochi/

### Maxima
# - requires docker arguments "--security-opt seccomp=unconfined"
ENV CCL_DEFAULT_DIRECTORY /opt/ccl
ENV QUICKLISP_HOME /opt/quicklisp
RUN cd /opt && \
    mkdir /root/.maxima && \
    mkdir -p /opt/quicklisp/local-projects && \
    pushd /opt/quicklisp/local-projects && \
    git clone https://github.com/robert-dodier/maxima-asdf.git && \
    popd && \
    wget https://github.com/Clozure/ccl/releases/download/v1.12/ccl-1.12-linuxx86.tar.gz && \
    tar xvfz ccl-1.12-linuxx86.tar.gz && \
    pushd ccl && \
    ./lx86cl64 --eval "(progn (rebuild-ccl :full t) (ccl::quit))" && \
    ln -s /opt/ccl/scripts/ccl /usr/bin/ccl && \
    ln -s /opt/ccl/scripts/ccl64 /usr/bin/ccl64 && \
    popd && \
    apt-get install -y rlwrap sbcl cl-quicklisp clisp clisp-module-pcre clisp-module-zlib && \
    wget http://beta.quicklisp.org/quicklisp.lisp && \
    echo "(load \"quicklisp.lisp\")" > /opt/instquick.lisp && \
    echo "(quicklisp-quickstart:install :path \"/opt/quicklisp\")" >> /opt/instquick.lisp && \
    echo "(quit)" >> /opt/instquick.lisp && \
    ccl64 --load /opt/instquick.lisp && \
    echo "#-quicklisp" > /root/.ccl-init.lisp && \
    echo "(let ((quicklisp-init #P\"/opt/quicklisp/setup.lisp\"))" >> /root/.ccl-init.lisp && \
    echo "  (when (probe-file quicklisp-init)" >> /root/.ccl-init.lisp && \
    echo "    (load quicklisp-init)))" >> /root/.ccl-init.lisp && \
    cp /root/.ccl-init.lisp /root/.sbclrc && \
    wget https://ayera.dl.sourceforge.net/project/maxima/Maxima-source/5.44.0-source/maxima-5.44.0.tar.gz && \
    tar xvfz maxima-5.44.0.tar.gz && \
    pushd maxima-5.44.0 && \
    ./configure --enable-sbcl-exec \
        --enable-ccl-exec \
        --with-ccl=ccl \
        --enable-ccl64-exec \
        --with-ccl64=ccl64 \
        --enable-clisp-exec \
        --with-default-lisp=sbcl && \
    make -j16 install && \
    echo "#-quicklisp" > /root/.maxima/maxima-init.lisp && \
    echo "(let ((quicklisp-init (merge-pathnames \"quicklisp/setup.lisp\" (user-homedir-pathname))))" >> /root/.maxima/maxima-init.lisp && \
    echo "  (when (probe-file quicklisp-init)" >> /root/.maxima/maxima-init.lisp && \
    echo "    (load quicklisp-init)))" >> /root/.maxima/maxima-init.lisp && \
    echo "(ql:quickload :drakma)" >> /root/.maxima/maxima-init.lisp && \
    echo "(ql:quickload :maxima-asdf)" >> /root/.maxima/maxima-init.lisp && \
    popd && \
    echo "alias maxima=\"rlwrap maxima\"" >> /etc/bash.bashrc && \
    git clone https://github.com/robert-dodier/maxima-jupyter.git && \
    pushd maxima-jupyter && \
    echo "load(\"load-maxima-jupyter.lisp\");" > /opt/maxima-jupyter/instmaxjup.mac && \
    echo "jupyter_install();" >> /opt/maxima-jupyter/instmaxjup.mac && \
    echo "quit();" >> /opt/maxima-jupyter/instmaxjup.mac && \
    maxima -b instmaxjup.mac && \
    popd

### Common LISP
ENV PATH /root/.roswell/bin:$PATH
RUN cd /opt && \
    echo "(ql:quickload :common-lisp-jupyter)" > instcommon.lisp && \
    echo "(cl-jupyter:install)" >> instcommon.lisp && \
    echo "(quit)" >> instcommon.lisp && \
    sbcl --load instcommon.lisp

### Yacas
RUN cd /opt && \
    apt-get install -y libboost-all-dev libjsoncpp1 libjsoncpp-dev && \
    git clone https://github.com/zeromq/zmqpp.git && \
    mkdir /opt/zmqpp/build && \
    pushd /opt/zmqpp/build && \
    cmake .. && \
    make -j16 install && \
    popd && \
    # use master for now, it has Plotting working
    git clone https://github.com/grzegorzmazur/yacas.git && \
    mkdir /opt/yacas/build && \
    pushd /opt/yacas/build && \
    cmake -DENABLE_CYACAS_CONSOLE:BOOL=ON \
        -DENABLE_CYACAS_GUI:BOOL=OFF \
        -DENABLE_CYACAS_GUI_PRIVATE_CODEMIRROR:BOOL=OFF \
        -DENABLE_CYACAS_GUI_PRIVATE_MATHJAX:BOOL=OFF \
        -DENABLE_CYACAS_KERNEL:BOOL=ON \
        .. && \
    make -j16 install && \
    mkdir /root/.local/share/jupyter/kernels/yacas && \
    echo "{ \"display_name\": \"Yacas\", \"language\": \"yacas\", \"argv\": [\"/usr/local/bin/yacas-kernel\",\"{connection_file}\",\"/opt/yacas/scripts\"]}" > /root/.local/share/jupyter/kernels/yacas/kernel.json && \
    popd
COPY ./icons/yacas/ /root/.local/share/jupyter/kernels/yacas/

### OCaml
RUN cd /opt && \
    apt-get install -y ocaml opam mccs && \
    opam init -a --dot-profile=/etc/bash.bashrc \
    --comp=ocaml-base-compiler.4.08.0 --disable-shell-hook \
    --solver=mccs --disable-sandboxing --reinit && \
    eval $(opam env) && \
    echo "test -r /root/.opam/opam-init/init.sh && . /root/.opam/opam-init/init.sh > /dev/null 2> /dev/null || true" >> /etc/bash.bashrc && \
    opam install -y graphics && \
    opam install -y cairo2 archimedes jupyter && \
    opam install -y jupyter-archimedes && \
    opam install -y cohttp cohttp-lwt-unix ppx_deriving_yojson && \
    wget http://download.openpkg.org/components/cache/ocaml-findlib/findlib-1.8.1.tar.gz && \
    tar xvf findlib-1.8.1.tar.gz && \
    pushd findlib-1.8.1 && \
    ./configure -bindir /usr/bin && make -j16 all && make -j16 opt && make -j16 install && \
    popd && \
    git clone https://github.com/akabe/ocaml-jupyter.git && \
    pushd ocaml-jupyter && \
    ./config/ocaml-jupyter-opam-genspec && \
    jupyter kernelspec install --name ocaml-jupyter "$(opam var share)/jupyter" && \
    popd
COPY ./icons/ocaml/ /usr/local/share/jupyter/kernels/ocaml-jupyter/

### Coq
RUN cd /opt && \
    eval $(opam env) && \
    opam repo add coq-released https://coq.inria.fr/opam/released && \
    opam install -y coq && \
    pip install coq-jupyter && \
    conda run python -m coq_jupyter.install
COPY ./icons/coq/ /usr/local/share/jupyter/kernels/coq/

### Rust
### evcxr
ENV PATH $PATH:/root/.cargo/bin
RUN cd /opt && \
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > install_rustup.sh && \
    chmod a+x install_rustup.sh && \
    ./install_rustup.sh -y && \
    git clone --branch v0.4.6 https://github.com/google/evcxr.git && \
    pushd evcxr/evcxr_jupyter && \
    cargo install --path . && \
    evcxr_jupyter --install && \
    popd

### Cadabra
RUN cd /opt && \
    conda install -y -c conda-forge glibmm zeromq cppzmq xtl sqlite util-linux \
    cryptopp xeus nlohmann_json sympy && \
    git clone --branch 2.3.0 https://github.com/kpeeters/cadabra2.git && \
    mkdir /opt/cadabra2/build && \
    pushd /opt/cadabra2/build && \
    cmake -DENABLE_JUPYTER=ON -DENABLE_FRONTEND=OFF \
        -DCMAKE_INCLUDE_PATH=/opt/conda/include \
        -DCMAKE_LIBRARY_PATH=/opt/conda/lib \
        -DCMAKE_INSTALL_PREFIX=/opt/conda \
        .. && \
    make -j16 install && \
    popd
COPY ./icons/cadabra/ /opt/conda/share/jupyter/kernels/cadabra/

### BeakerX
### Java
### Groovy
### Scala
### Clojure
### SQL
RUN cd /opt && \
    conda install -y -c conda-forge openjdk=8.0.121 maven py4j pyspark bottle yarn=1.19.1 && \
    git clone --branch lukasz/8246 https://github.com/twosigma/beakerx.git && \
    pushd beakerx && \
      pushd beakerx && \
          pip install -r requirements.txt && \
      popd && \
      beakerx install && \
      jupyter nbextension enable beakerx_tabledisplay --py --sys-prefix && \
      beakerx_databrowser install && \
      jupyter nbextension enable beakerx_databrowser --py --sys-prefix && \
      pushd js/lab && \
          jupyter labextension install . --no-build && \
      popd && pushd js/lab-theme-dark && \
          jupyter labextension install . --no-build && \
      popd && pushd js/lab-theme-light && \
          jupyter labextension install . --no-build && \
      popd && \
      jupyter lab build && \
      # we're going to use the regular Kotlin plugin
      rm -rf /opt/conda/share/jupyter/kernels/kotlin && \
    popd

### Kotlin
RUN cd /opt && \
    conda install -y -c jetbrains kotlin-jupyter-kernel

### NodeJS
RUN cd /opt && \
    # TODO: don't install IJavascript at top
    rm -rf /root/.local/share/jupyter/kernels/javascript && \
    git clone https://github.com/3Nigma/nelu-kernelu.git && \
    pushd nelu-kernelu && \
    npm install . && \
    popd

### Guile
RUN cd /opt && \
    apt-get install -y libgc-dev libunistring-dev && \
    wget ftp://ftp.gnu.org/gnu/guile/guile-2.0.12.tar.gz && \
    tar xvf guile-2.0.12.tar.gz && \
    pushd guile-2.0.12 && \
    ./configure && \
    make -j16 install && \
    git clone https://github.com/jerry40/guile-kernel.git && \
    sed -i "s|guile-kernel/src/||g" guile-kernel/src/kernel.json && \
    mv guile-kernel/src /usr/local/share/jupyter/kernels/guile && \
    wget http://download.savannah.gnu.org/releases/guile-json/guile-json-3.2.0.tar.gz && \
    tar xvf guile-json-3.2.0.tar.gz && \
    pushd guile-json-3.2.0 && \
        ./configure --prefix=/usr/local && \
        make -j16 install && \
    popd && \
    mkdir -p /usr/local/share/guile/site && \
    pushd /usr/local/share/guile/site && \
        wget https://raw.githubusercontent.com/jerry40/guile-simple-zmq/master/src/simple-zmq.scm && \
    popd && \
    popd

### Processing
# NOTE: uses old processing.js, not new p5.js
ENV PATH $PATH:/opt/processing-3.5.4
ENV PROCESSING_JAVA /opt/processing-3.5.4/processing-java
RUN cd /opt && \
    wget https://download.processing.org/processing-3.5.4-linux64.tgz && \
    tar xvf processing-3.5.4-linux64.tgz && \
    pip install --upgrade calysto_processing && \
    conda run python -m calysto_processing install && \
    sed -i "s|Calysto Processing|Processing|g" /usr/local/share/jupyter/kernels/calysto_processing/kernel.json
COPY ./icons/processing/ /usr/local/share/jupyter/kernels/calysto_processing/

### CWL
### Common Workspace Language
RUN cd /opt && \
    conda install -y -c conda-forge pydot cwltool && \
    git clone -b v.0.0.1 https://github.com/giannisdoukas/CWLJNIKernel.git && \
    pushd CWLJNIKernel && \
      pip install -e . && \
      cp -r ./kernelmeta /usr/local/share/jupyter/kernels/cwlkernel && \
    popd

### MIPS
RUN cd /opt && \
    apt-get install -y spim && \
    git clone https://github.com/epalmese/MIPS-jupyter-kernel.git && \
    pushd MIPS-jupyter-kernel/kernel && \
    . install_kernel.sh && \
    cp mips_kernel.py /opt/conda/lib/python3.7/site-packages/ && \
    popd

### Emu86
RUN cd /opt && \
    pip install emu86 && \
    conda run python -m kernels.intel.install
COPY ./icons/emu86/ /root/.local/share/jupyter/kernels/intel/

### Coarray Fortran
### Fortran
RUN cd /opt && \
    apt-get install -y mpich libmpich-dev libmpich12 libcoarrays-dev libcoarrays-mpich-dev && \
    wget https://github.com/sourceryinstitute/OpenCoarrays/releases/download/2.8.0/OpenCoarrays-2.8.0.tar.gz && \
    tar xvf OpenCoarrays-2.8.0.tar.gz && \
    mkdir -p /opt/OpenCoarrays-2.8.0/ocbuild && \
    pushd OpenCoarrays-2.8.0/ocbuild && \
    FC=/usr/bin/gfortran \
        CC=/usr/bin/gcc \
        cmake -DCMAKE_INSTALL_PREFIX=/usr/local \
        -DMPIEXEC_PREFLAGS="--allow-run-as-root" \
        .. && \
    make -j16 install && \
    popd && \
    git clone https://github.com/sourceryinstitute/jupyter-CAF-kernel && \
    pushd jupyter-CAF-kernel && \
    pip install -e prebuild/jupyter-caf-kernel && \
    jupyter kernelspec install prebuild/jupyter-caf-kernel/Coarray-Fortran/ && \
    popd
COPY ./icons/fortran/ /usr/local/share/jupyter/kernels/coarray-fortran/

### Pharo
### Smalltalk
# requires docker arguments "--ulimit rtprio=2:2"
ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:/usr/lib:/usr/lib/x86_64-linux-gnu
RUN cd /opt && \
    wget https://storage.googleapis.com/tensorflow/libtensorflow/libtensorflow-cpu-linux-x86_64-1.13.1.tar.gz && \
    pushd /usr/local && \
    tar xvf /opt/libtensorflow-cpu-linux-x86_64-1.13.1.tar.gz && \
    popd && \
    mkdir /opt/pharo && \
    pushd /opt/pharo && \
    wget https://files.pharo.org/get-files/80/pharo64.zip && \
    unzip pharo64.zip && \
    wget https://files.pharo.org/get-files/80/pharo64-linux-stable.zip && \
    unzip pharo64-linux-stable.zip && \
    mv `ls *.image | head -1` Pharo8.0.image && \
    ./pharo -headless Pharo8.0.image eval --save \
        "Metacello new repository: 'github://svenvc/NeoCSV/repository'; baseline: 'NeoCSV'; load." && \
    ./pharo -headless Pharo8.0.image eval --save \
        "Metacello new repository: 'github://ObjectProfile/Roassal2/src'; baseline: 'Roassal2'; load." && \
    ./pharo -headless Pharo8.0.image eval --save \
        "Metacello new repository: 'github://PolyMathOrg/DataFrame:v2.0/src'; baseline: 'DataFrame'; load." && \
    ./pharo -headless Pharo8.0.image eval --save \
        "Metacello new githubUser: 'PolyMathOrg' project: 'libtensorflow-pharo-bindings' commitish: 'master' path: ''; baseline: 'LibTensorFlowPharoBinding'; load." && \
    ./pharo -headless Pharo8.0.image metacello install \
        github://birnam/JupyterTalkRepository:hardcode_kernelsdir/repository \
        BaselineOfJupyterTalk && \
    popd
COPY ./icons/pharo/ /opt/conda/share/jupyter/kernels/pharo/

### agda
RUN cd /opt && \
    apt-get install -y agda && \
    pip install agda_kernel && \
    conda run python -m agda_kernel.install && \
    git clone https://github.com/lclem/jupyter_contrib_nbextensions.git && \
    pushd jupyter_contrib_nbextensions && \
      pip install -e . && \
      jupyter contrib nbextension install --user && \
      jupyter nbextension enable --py widgetsnbextension && \
      jupyter nbextension enable agda-extension/main && \
      jupyter nbextension enable toc2/main && \
    popd
COPY ./icons/agda/ /usr/local/share/jupyter/kernels/agda/

### attempted, but ultimately failed to install:
# - Purescript
# - Brainfuck
# - TaQL / Casacore -- revisit one day using example here: https://github.com/aardk/jupyter-casa/blob/master/docker/Dockerfile
# - D -- not sure how jupyter-wire is supposed to be a D kernel, or be a kernel in D
# - Turtle -- found a good nbextension but not compatible with lab
# - Skulpt -- javascript error (might work in old notebook)
# - IVisual (VPython) -- javascript error (works in old notebook)
# - SWI-Prolog -- needs kernel rewrite
# - SPARQL -- depends on a SPARQL endpoint
# - Cryptol -- icryptol made for older ipython, needs major overhaul
# - Aldor -- couldn't get past missing standard library files

### other kernels that aren't included because of paywall
# - Matlab
# - Wolfram (required Mathematica / Wolfram Engine)
# - IDL (GDL is an open source alternative)
# - Stata
# - Dyalog APL
# - SAS
# - Stata
# - Teradata
# - Kdb+/Q (there is a free version but must be individually requested)

### kernels that aren't included because they rely on another service (e.g. database)
# - Sparkmagic (requires a setup Spark cluster)
# - HiveQL (requires Hive access)
# - MongoDB (requires mongodb server)

### kernels that aren't included because of hardware requirements
# - Torch (no longer developed, relies on GPU)
# - CircuitPython (requires dev board)
# - MicroPython (requires dev board)

### other kernels that aren't included because of duplication or other reason
# - PeForth (not a language but a debugging syntax for Python)
# - Ansible (not a language as much as a configuration)
# - Calico (more of an IDE, it's also a way of accessing multiple languages)
# - SBT (a build script not a language)
# - SciJava (a polyglot kernel that replicates many others already included)
# - Bacata/Rascal (metaprogramming, used to generate other kernels, unnecessary)
# - Spylon (duplicates Scala and Spark from BeakerX)
# - jp-babel (a javascript compiler, so no different than other js)
# - StuPyd (wait for the language to mature)

EXPOSE 8888
COPY ./base/config/jupyter_notebook_config.py /root/.jupyter/jupyter_notebook_config.py
COPY ./base/config/.vimrc /root/.vimrc

# CMD ["/opt/conda/bin/jupyter", "notebook", "--notebook-dir=/opt/notebooks", "--local_hostnamesList=['localhost']", "--ip='*'", "--port=8888", "--no-browser", "--allow-root", "--NotebookApp.token=''"]
# CMD ["/opt/conda/bin/jupyter", "notebook", "--ip=0.0.0.0", "--port=8888", "--allow-root"]
# CMD ["/opt/conda/bin/jupyter", "notebook"]
CMD ["/bin/bash"]

# CMD ["jupyter", "notebook"]
