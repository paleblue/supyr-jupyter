{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Playing around with Vega-Lite in IHaskell and JupyterLab\n",
    "\n",
    "This is a *minimal* example showing how we can take advantage of the native support\n",
    "for Vega-Lite visualizations in Jupyter Lab with IHaskell. The code also works in\n",
    "Jupyter notebooks, but it is not quite \"as good\", since there is no PNG version\n",
    "(so you don't have a representation of the plot when viewing the notebook outside\n",
    "of the browser), and the code relies on a number of external Javascript libraries.\n",
    "\n",
    "For the moment the `ihaskell-hvega` display program still defaults to supporting\n",
    "the \"notebook\" rather than \"lab\" environment, hence the need to explicitly use the\n",
    "`vlShow` function rather than it happening automatically.\n",
    "\n",
    "## Running this notebook\n",
    "\n",
    "This notebook uses [Tweag's jupyterWith environment](https://github.com/tweag/jupyterWith).\n",
    "If you have `nix` installed then you should be able to use\n",
    "[`shell.nix`](https://github.com/DougBurke/hvega/blob/master/notebooks/shell.nix) to\n",
    "create the necessary environment with:\n",
    "\n",
    "    % nix-shell --command \"jupyter lab\"\n",
    "    \n",
    "The last time the notebook was run, the following packages were used:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ":!ghc-pkg latest ghc\n",
    ":!ghc-pkg latest ihaskell\n",
    ":!ghc-pkg latest hvega\n",
    ":!ghc-pkg latest ihaskell-hvega\n",
    ":opt no-lint"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Let's start at the beginning\n",
    "\n",
    "We are going to start this notebook by loading in the `hvega` module (which provides\n",
    "a pretty-direct encoding of the Vega-Lite schema into Haskell, and is based on the\n",
    "[Elm Vega library](http://package.elm-lang.org/packages/gicentre/elm-vega/2.2.1/VegaLite)\n",
    "by Jo Wood. There are some potential conflicts with other Haskell routines, such\n",
    "as `filter`, hence the qualified import. Note that `IHaskell` automtically loads the\n",
    "display code from the `ihaskell-hvega` package (if installed), which is where `vlShow`\n",
    "comes from (the import of this is left as a comment as a reminder to me)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ":ext OverloadedStrings"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import qualified Graphics.Vega.VegaLite as VL\n",
    "import Graphics.Vega.VegaLite hiding (filter)\n",
    "\n",
    "-- import IHaskell.Display.Hvega (vlShow)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are plenty of [awesome Vega-Lite examples](https://vega.github.io/vega-lite/examples/) but\n",
    "I came up with the following when developing `hvega` and have stuck with it. I may well have to\n",
    "rethink this!\n",
    "\n",
    "First we create a Vega-Lite vizualization (`vl1`) using the\n",
    "[`Graphics.Vega.VegaLite` module](https://hackage.haskell.org/package/hvega-0.1.0.3/docs/Graphics-Vega-VegaLite.html):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vl1 = \n",
    "  let desc = \"A very exciting bar chart\"\n",
    "\n",
    "      dat = dataFromRows [Parse [(\"start\", FoDate \"%Y-%m-%d\")]]\n",
    "            . dataRow [(\"start\", Str \"2011-03-25\"), (\"count\", Number 23)]\n",
    "            . dataRow [(\"start\", Str \"2011-04-02\"), (\"count\", Number 45)]\n",
    "            . dataRow [(\"start\", Str \"2011-04-12\"), (\"count\", Number 3)]\n",
    "\n",
    "      barOpts = [MOpacity 0.4, MColor \"teal\"]\n",
    "\n",
    "      enc = encoding\n",
    "            . position X [PName \"start\", PmType Temporal, PAxis [AxTitle \"Inception date\"]]\n",
    "            . position Y [PName \"count\", PmType Quantitative]\n",
    "            \n",
    "  in toVegaLite [description desc, background \"white\", dat [], mark Bar barOpts, enc []]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This has type `VegaLite`, which I would like to have auto-displayed, but as shown in cell 4 below,\n",
    "the code from `IHaskell.Display.Vega` - which results in the visualization being displayed in a\n",
    "Jupyter notebook - fails here: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    ":t vl1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vl1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From within a browser you get the error `Javascript Error: requirejs is not defined`, and external viewers\n",
    "see no output.\n",
    "\n",
    "As a (hopefully) short-term work-around, I have the `vlShow` helper,\n",
    "which converts the `VegaLite` type into one that can be automatically-displayed by Jupyter Lab:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vlShow vl1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "My, what a particularly-awesome visualization! When viewed within the Jupyter Lab browser, there\n",
    "is a menu of items to the top-right of the visualization which lets you save the visualization as\n",
    "`SVG` or `PNG`, or interact with the Vega-Lite representation directly.\n",
    "\n",
    "Viewers reading the notebook \"outside\" of a browser - such as with\n",
    "[GitHub's ipynb viewer](https://github.com/DougBurke/hvega/blob/master/notebooks/vegalite.ipynb) - will\n",
    "see a PNG version of the visualization. For this (and the following) example there is\n",
    "not-uch difference, but it makes more of a difference with the third plot,\n",
    "which loses all the interactive interactivity in the PNG version ;-)\n",
    "\n",
    "## A more-interesting example\n",
    "\n",
    "We can show a bit-more power of Vega Lite: reading data from an external URL and doing a little-bit of\n",
    "data-munging (this example is based on https://vega.github.io/vega-lite/examples/line.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "line =\n",
    "    let dvals = dataFromUrl \"https://vega.github.io/vega-datasets/data/stocks.csv\"\n",
    "        -- before version 0.4.0.0 title jjst took a string\n",
    "        label = title \"Google's stock price over time.\" []\n",
    "           \n",
    "        trans = transform\n",
    "                   . VL.filter (FExpr \"datum.symbol==='GOOG'\")\n",
    "                   \n",
    "        enc = encoding\n",
    "                 . position X [PName \"date\", PmType Temporal, PAxis [AxFormat \"%Y\"]]\n",
    "                 . position Y [PName \"price\", PmType Quantitative]\n",
    "\n",
    "    in toVegaLite [label, width 400, height 400, dvals [], trans [], mark Line [], enc []]\n",
    "       \n",
    "vlShow line"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, the Vega-Lite data format is JSON, and the `VLSpec` type is just a simple wrapper around Aeson's `Value` type, so we can see the actual Vega-Lite specification with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import Data.Aeson (encode)\n",
    "import qualified Data.ByteString.Lazy.Char8 as BL\n",
    "\n",
    "BL.putStrLn (encode (fromVL line))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we write this out to a file, then you can display it directly with Jupyter Lab: it should appear in the file browser and double-clicking it will creata a new tab with the visualization (I think you can use either `.vl` or `.json.vl` as the file extension)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "BL.writeFile \"line.json.vl\" (encode (fromVL line))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Perhaps I need to add some helper routines to simplify this!\n",
    "\n",
    "Version `0.2.1.0` of `hvega` introduced the `toHtml` and `toHtmlFile` routines, which\n",
    "create a HTML page which can be viewed in a web browser (and uses the JavaScript\n",
    "[Vega Embed](https://vega.github.io/vega-lite/usage/embed.html) package)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "toHtml line"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Interactivity rules\n",
    "\n",
    "How about something a-bit-more interactive? This example is taken from\n",
    "https://vega.github.io/vega-lite/examples/interactive_multi_line_tooltip.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "interactiveMultiLineTooltip =\n",
    "    let dvals = dataFromUrl \"https://vega.github.io/vega-datasets/data/stocks.csv\"\n",
    "                   [CSV, Parse [(\"date\", FoDate \"\")]]\n",
    "           \n",
    "        enc = encoding\n",
    "                 . position X [PName \"date\", PmType Temporal]\n",
    "                 . position Y [PName \"price\", PmType Quantitative]\n",
    "                 . color [MName \"symbol\", MmType Nominal]\n",
    "\n",
    "        enc12 = encoding\n",
    "                   . opacity [ MSelectionCondition (SelectionName \"tooltip\")\n",
    "                               [MNumber 1] [MNumber 0]\n",
    "                             ]\n",
    "           \n",
    "        lyr1 = asSpec [enc [], layer [lyr11, lyr12]]\n",
    "        lyr11 = asSpec [mark Line []]\n",
    "        lyr12 = asSpec [sel12 [], mark Point [], enc12 []]\n",
    "        sel12 = selection\n",
    "                   . select \"tooltip\" Single [Nearest True, On \"mouseover\", Empty, Encodings [ChX]]\n",
    "           \n",
    "        trans2 = transform\n",
    "                    . VL.filter (FSelection \"tooltip\")\n",
    "        \n",
    "        posX = position X [PName \"date\", PmType Temporal]\n",
    "        enc21 = encoding\n",
    "                   . posX\n",
    "        enc22 = encoding\n",
    "                   . posX\n",
    "                   . position Y [PName \"price\", PmType Quantitative]\n",
    "                   . text [TName \"price\", TmType Quantitative]\n",
    "                   . color [MName \"symbol\", MmType Nominal]\n",
    "                   \n",
    "        lyr21 = asSpec [mark Rule [MColor \"gray\"], enc21 []]\n",
    "        lyr22 = asSpec [mark VL.Text [MAlign AlignLeft, MdX 5, MdY (-5)], enc22 []]\n",
    "           \n",
    "        lyr2 = asSpec [trans2 [], layer [lyr21, lyr22]]\n",
    "           \n",
    "    in toVegaLite [dvals, width 800, height 400, layer [lyr1, lyr2]]\n",
    "       \n",
    "vlShow interactiveMultiLineTooltip"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For those not viewing this directly in the notebook, a vertical bar is displayed as you move across the visualization, displaying the numerical values (the `price` values) of the curves at that point. \n",
    "\n",
    "And how does Jupyter Lab deal with this visualization when written out as a file?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "BL.writeFile \"interactive.json.vl\" (encode (fromVL interactiveMultiLineTooltip))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Yay; interactive overload ahoy (if you double-click on `interactive.json.vl` in the file browser part of Jupyter Lab)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Haskell",
   "language": "haskell",
   "name": "haskell"
  },
  "language_info": {
   "codemirror_mode": "ihaskell",
   "file_extension": ".hs",
   "name": "haskell",
   "pygments_lexer": "Haskell",
   "version": "8.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
