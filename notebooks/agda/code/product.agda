                       

    
module code.product where

infixr 4 _,_
record Σ {l} (A : Set l) (B : A → Set) : Set l where
    constructor _,_
    field
      fst : A
      snd : B fst

-- non-dependent product
infixr 2 _∧_
_∧_ : Set → Set → Set
A ∧ B = Σ A (λ _ → B)

infix 0 thereExists
thereExists : ∀ {l} {A : Set l} (B : A → Set) → Set l
thereExists {_} {A} B = Σ A B

syntax thereExists (λ x → B) = ∃[ x ] B
    
