              

    
module code.neg where

data ⊥ : Set where

⊥-elim : {A : Set} → ⊥ → A
⊥-elim ()

infix 3 ¬_ -- higher priority than ∨ and ∧

¬_ : Set → Set
¬ A = A → ⊥
    
