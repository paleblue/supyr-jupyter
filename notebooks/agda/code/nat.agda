                     

    
module code.nat where
open import code.eq public

data ℕ : Set where 
  zero : ℕ
  suc : ℕ → ℕ
  
{-# BUILTIN NATURAL ℕ #-}

infixl 5 _+_
_+_ : ℕ → ℕ → ℕ
zero + m = m
(suc n) + m = suc (n + m)

infixl 6 _*_
_*_ : ℕ → ℕ → ℕ
zero * m = zero
(suc n) * m = m + (n * m)

postulate +-comm : (m n : ℕ) → m + n ≡ n + m

+-comm-auto : ∀ {m n} → m + n ≡ n + m
+-comm-auto {m} {n} = +-comm m n 

infix 4 _≤_
data _≤_ : ℕ → ℕ → Set where
  0≤n : {n : ℕ} → 0 ≤ n
  s≤s : {m n : ℕ} → m ≤ n → suc m ≤ suc n
  
≤-trans : ∀ {o m n} → o ≤ m → m ≤ n → o ≤ n
≤-trans 0≤n m≤n = 0≤n
≤-trans (s≤s o≤m) (s≤s m≤n) = s≤s (≤-trans o≤m m≤n)

≤-+-left : ∀ {m1 m2} → m1 ≤ m1 + m2
≤-+-left {zero} = 0≤n
≤-+-left {suc m1} = s≤s ≤-+-left

≤-+-right : ∀ {m1 m2} → m2 ≤ m1 + m2
≤-+-right {m1} {m2} = subst (m2 ≤_) (sym (+-comm m1 m2)) (≤-+-left {m2} {m1})

suc-lemma : (m n : ℕ) → suc (m + n) ≡ m + suc n
suc-lemma 0 n =
    suc (0 + n) ≡⟨⟩
    suc n ≡⟨⟩
    0 + suc n ∎
suc-lemma (suc m) n = 
    suc (suc m + n) ≡⟨⟩
    suc (suc (m + n)) ≡⟨ cong suc (suc-lemma m n) ⟩ 
    suc (m + suc n) ≡⟨⟩
    suc m + suc n ∎

-- Agda 2.5.3 has a problem with these ones
-- Agda 2.6 works OK
postulate ≤-suc2 : ∀ {m n} → suc m ≤ n → m ≤ n
--≤-suc2 sm≤n = ≤-trans ≤-+-right sm≤n

postulate ≤-+-cong-1 : ∀ {a b c : ℕ} → a ≤ c → a + b ≤ c + b
--≤-+-cong-1 0≤n = ≤-+-right
--≤-+-cong-1 (s≤s a≤c) = s≤s (≤-+-cong-1 a≤c)

postulate ≤-+-cong-2 : ∀ {a b c : ℕ} → b ≤ c → a + b ≤ a + c
{-≤-+-cong-2 {a} {b} {c} b≤c with +-comm b a | +-comm c a
... | p | q =
    subst (a + b ≤_) q
        (subst (_≤ c + a) p (≤-+-cong-1 {b = a} b≤c)) -}

postulate ≤-+-cong : ∀ {a b c d : ℕ} → a ≤ c → b ≤ d → a + b ≤ c + d
{- ≤-+-cong a≤c b≤d = ≤-trans (≤-+-cong-1 a≤c) (≤-+-cong-2 b≤d) -}

infix 4 _<_
_<_ : ℕ → ℕ → Set
m < n = suc m ≤ n

infix 4 _>_
_>_ : ℕ → ℕ → Set
m > n = n < m
    
