#!/bin/bash
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local \
    -DPYTHON3_EXECUTABLE=/opt/conda/bin/python \
    -DOPENCV_EXTRA_MODULES_PATH=/opt/opencv/opencv_contrib/modules \
    .. && \
make && make install && \
pip install imutils opencv-python

# pushd /opt && \
# mv /usr/lib/x86_64-linux-gnu/libgio-2.0.so{,.bak} && \
# mv /usr/lib/x86_64-linux-gnu/libgio-2.0.so.0{,.bak} && \
# mv /usr/lib/x86_64-linux-gnu/libgio-2.0.so.0.5800.3{,.bak} && \
# mv /usr/lib/x86_64-linux-gnu/libgthread-2.0.so{,.bak} && \
# mv /usr/lib/x86_64-linux-gnu/libgthread-2.0.so.0{,.bak} && \
# mv /usr/lib/x86_64-linux-gnu/libgthread-2.0.so.0.5800.3{,.bak} && \
# mv /usr/lib/x86_64-linux-gnu/libglib-2.0.so{,.bak} && \
# mv /usr/lib/x86_64-linux-gnu/libglib-2.0.so.0{,.bak} && \
# mv /usr/lib/x86_64-linux-gnu/libglib-2.0.so.0.5800.3{,.bak} && \
# mv /usr/lib/x86_64-linux-gnu/libfreetype.so{,.bak} && \
# mv /usr/lib/x86_64-linux-gnu/libfreetype.so.6{,.bak} && \
# mv /usr/lib/x86_64-linux-gnu/libfreetype.so.6.16.1{,.bak} && \
# mv /usr/lib/x86_64-linux-gnu/libfontconfig.so{,.bak} && \
# mv /usr/lib/x86_64-linux-gnu/libfontconfig.so.1{,.bak} && \
# mv /usr/lib/x86_64-linux-gnu/libfontconfig.so.1.12.0{,.bak} && \
# mkdir -p /opt/opencv/opencv/build && \
# pushd /opt/opencv/opencv/build && \
# cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local \
    # -Dpkgcfg_lib_GTK2_gtk-x11-2.0=/usr/lib/x86_64-linux-gnu/libgtk-x11-2.0.so \
    # -Dpkgcfg_lib_GTK2_gdk-x11-2.0=/usr/lib/x86_64-linux-gnu/libgdk-x11-2.0.so \
    # -Dpkgcfg_lib_GTK2_pangocairo-1.0=/usr/lib/x86_64-linux-gnu/libpangocairo-1.0.so \
    # -Dpkgcfg_lib_GTK2_atk-1.0=/usr/lib/x86_64-linux-gnu/libatk-1.0.so \
    # -Dpkgcfg_lib_GTK2_cairo=/usr/lib/x86_64-linux-gnu/libcairo.so \
    # -Dpkgcfg_lib_GTK2_gdk_pixbuf-2.0=/usr/lib/x86_64-linux-gnu/libgdk_pixbuf-2.0.so \
    # -Dpkgcfg_lib_GTK2_pangoft2-1.0=/usr/lib/x86_64-linux-gnu/libpangoft2-1.0.so \
    # -Dpkgcfg_lib_GTK2_pango-1.0=/usr/lib/x86_64-linux-gnu/libpango-1.0.so \
    # -Dpkgcfg_lib_GTK2_glib-2.0=/opt/conda/lib/libglib-2.0.so.0 \
    # -Dpkgcfg_lib_GTK2_gio-2.0=/opt/conda/lib/libgio-2.0.so \
    # -Dpkgcfg_lib_GTK2_gthread-2.0=/opt/conda/lib/libgthread-2.0.so \
    # -Dpkgcfg_lib_GTK2_gobject-2.0=/opt/conda/lib/libgobject-2.0.so \
    # -Dpkgcfg_lib_FREETYPE_freetype=/opt/conda/lib/libfreetype.so \
    # -Dpkgcfg_lib_FFMPEG_avcodec=/usr/lib/x86_64-linux-gnu/libavcodec.so \
    # -Dpkgcfg_lib_FFMPEG_avformat=/usr/lib/x86_64-linux-gnu/libavformat.so \
    # -Dpkgcfg_lib_FFMPEG_avutil=/usr/lib/x86_64-linux-gnu/libavutil.so \
    # -Dpkgcfg_lib_FFMPEG_swscale=/usr/lib/x86_64-linux-gnu/libswscale.so \
    # -Dpkgcfg_lib_HARFBUZZ_harfbuzz=/usr/lib/x86_64-linux-gnu/libharfbuzz.so \
    # -DOPENCV_EXTRA_MODULES_PATH=/opt/opencv/opencv_contrib/modules \
    # -DWITH_1394=OFF -DBUILD_OPENCV_HIGHGUI=OFF \
    # -DPYTHON2_EXECUTABLE=/usr/bin/python2.7 \
    # -DPYTHON2_LIBRARY=/usr/lib/python2.7 \
    # -DPYTHON2_INCLUDE_DIR=/usr/include/python2.7 \
    # -DPYTHON3_EXECUTABLE=/opt/conda/bin/python \
    # -DPYTHON3_INCLUDE_DIR=/usr/include/python3.7 \
    # -DPYTHON3_LIBRARY=/opt/conda/lib/python3.7 \
    # -DPYTHON3_NUMPY_INCLUDE_DIRS=/opt/conda/lib/python3.7/site-packages/numpy/core/include/numpy \
    # .. && \
# make && make install && \
# popd && popd && \
# pip install imutils opencv-python
