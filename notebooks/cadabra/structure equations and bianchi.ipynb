{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cartan structural equations and Bianchi identity\n",
    "Oscar Castillo-Felisola"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Theoretical background"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let $M$ be a manifold, and $g$ a (semi)Riemannian metric defined on $M$. Then the line element for the metric $g$ is\n",
    "$$\\begin{equation*}\n",
    "  \\mathrm{d}{s}^2(g) = g_{\\mu\\nu} \\mathrm{d}{x}^\\mu \\otimes \\mathrm{d}{x}^\\nu.\n",
    "\\end{equation*}$$\n",
    "Nonetheless, the information about the metric structure of the manifold can be translated to the language of frames,\n",
    "$$\\begin{equation*}\n",
    "  \\begin{split}\n",
    "    \\mathrm{d}{s}^2(g)\n",
    "    & = g_{\\mu\\nu} \\mathrm{d}{x}^\\mu \\otimes \\mathrm{d}{x}^\\nu \\\\\n",
    "    & = \\eta_{ab} \\; e^{a}_{\\mu}(x) \\, e^{b}_{\\nu}(x) \\; \\mathrm{d}{x}^\\mu \\otimes \\mathrm{d}{x}^\\nu \\\\\n",
    "    & = \\eta_{ab} \\; \\mathrm{e}^{a} \\otimes \\mathrm{e}^{b}.\n",
    "  \\end{split}\n",
    "\\end{equation*}$$\n",
    "Therefore, the vielbein 1-form, $\\mathrm{e}^{a} \\equiv e^{a}_{\\mu}(x) \\mathrm{d}{x}^\\mu$, encodes the information of the metric tensor. In order to complete the structure, one needs information about the transport of geometrical objects lying on bundles based on $M$. That information is encoded on the spin connection 1-form, $\\omega^{a}{}_{b}$. Using these quantities one finds the generalisation of the structure equations of Cartan,\n",
    "$$\\begin{align}\n",
    "  \\mathrm{d}{\\mathrm{e}^{a}} + \\omega^{a}{}_{b} \\wedge \\mathrm{e}^{b} & = \\mathrm{T}^{a},\n",
    "  \\label{firstSE}\\\\\n",
    "  \\mathrm{d}{\\omega^{a}{}_{c}} + \\omega^{a}{}_{b} \\wedge \\omega^{b}{}_{c} & = \\mathrm{R}^{a}{}_{c}\n",
    "  \\label{secondSE}.\n",
    "\\end{align}$$\n",
    "The torsion 2-form, $\\mathrm{T}^{a}$, and the curvature 2-form, $\\mathrm{R}^{a}{}_{c}$, measure the impossibility of endowing $M$ with an Euclidean structure."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Manipulation of the structural equations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Defintions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/markdown": [
       "${}\\text{Attached property DifferentialForm to~}\\left[e^{a},~ \\omega^{a}\\,_{b}\\right].$"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "text/markdown": [
       "${}\\text{Attached property DifferentialForm to~}\\left[\\mathrm{T}^{a},~ \\mathrm{R}^{a}\\,_{b}\\right].$"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "{a,b,c,l,m,n}::Indices.\n",
    "d{#}::ExteriorDerivative;.\n",
    "d{#}::LaTeXForm(\"\\mathrm{d}\").\n",
    "T{#}::LaTeXForm(\"\\mathrm{T}\").\n",
    "R{#}::LaTeXForm(\"\\mathrm{R}\").\n",
    "{e^{a}, \\omega^{a}_{b}}::DifferentialForm(degree=1); \n",
    "{T^{a}, R^{a}_{b}}::DifferentialForm(degree=2);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Cartan structural equations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/markdown": [
       "${}\\mathrm{d}{e^{a}}+\\omega^{a}\\,_{b}\\wedge e^{b}-\\mathrm{T}^{a} = 0$"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d{#}::Accent.\n",
    "struc1 := d{e^{a}} + \\omega^{a}_{b} ^ e^{b} - T^{a} = 0;"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/markdown": [
       "${}\\mathrm{d}{\\omega^{a}\\,_{b}}+\\omega^{a}\\,_{m}\\wedge \\omega^{m}\\,_{b}-\\mathrm{R}^{a}\\,_{b} = 0$"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "struc2 := d{\\omega^{a}_{b}} + \\omega^{a}_{m} ^ \\omega^{m}_{b} - R^{a}_{b} = 0;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the following, we will also use the structural equations as definitions of the exterior derivatives of the vielbein and spin connection 1-forms. Therefore, we shall utilise the isolate algorithm---from the cdb.core.manip library---to define substitution rules."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/markdown": [
       "${}\\mathrm{d}{e^{a}} = -\\omega^{a}\\,_{b}\\wedge e^{b}+\\mathrm{T}^{a}$"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from cdb.core.manip import *\n",
    "de:= @(struc1):\n",
    "isolate(de, $d{e^{a}}$);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/markdown": [
       "${}\\mathrm{d}{\\omega^{a}\\,_{b}} = -\\omega^{a}\\,_{m}\\wedge \\omega^{m}\\,_{b}+\\mathrm{R}^{a}\\,_{b}$"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "domega := @(struc2):\n",
    "isolate(domega, $d{\\omega^{a}_{b}}$);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bianchi identities\n",
    "\n",
    "The bianchi identities are obtained by applying the exterior derivative to the structural equations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### First Bianchi identity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/markdown": [
       "${}\\mathrm{d}{\\left(\\mathrm{d}{e^{a}}+\\omega^{a}\\,_{b}\\wedge e^{b}-\\mathrm{T}^{a} = 0\\right)}$"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Bianchi1 :=  d({ @(struc1) });"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/markdown": [
       "${}\\mathrm{d}{\\left(\\mathrm{d}{e^{a}}+\\omega^{a}\\,_{b}\\wedge e^{b}-\\mathrm{T}^{a} = 0\\right)}$"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "distribute(Bianchi1)\n",
    "product_rule(_);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/markdown": [
       "${}\\mathrm{d}{\\left(0 = 0\\right)}$"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "substitute(Bianchi1, de, repeat=True)\n",
    "substitute(Bianchi1, domega, repeat=True)\n",
    "distribute(_);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/markdown": [
       "${}\\mathrm{d}{\\left(0 = 0\\right)}$"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "rename_dummies(Bianchi1);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the absence of torsion, the above expression is the well-known _algebraic_ Bianchi identity\n",
    "$$\\begin{equation*}\n",
    "R^\\mu{}_{\\nu\\lambda\\rho} + R^\\mu{}_{\\lambda\\rho\\nu} + R^\\mu{}_{\\rho\\nu\\lambda} = 0.\n",
    "\\end{equation*}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Second Bianchi identity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/markdown": [
       "${}\\mathrm{d}{\\left(\\mathrm{d}{\\omega^{a}\\,_{b}}+\\omega^{a}\\,_{m}\\wedge \\omega^{m}\\,_{b}-\\mathrm{R}^{a}\\,_{b} = 0\\right)}$"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "Bianchi2 := d({ @(struc2) });"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/markdown": [
       "${}\\mathrm{d}{\\left(\\mathrm{d}{\\omega^{a}\\,_{b}}+\\omega^{a}\\,_{m}\\wedge \\omega^{m}\\,_{b}-\\mathrm{R}^{a}\\,_{b} = 0\\right)}$"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "distribute(Bianchi2)\n",
    "product_rule(_);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/markdown": [
       "${}\\mathrm{d}{\\left(0 = 0\\right)}$"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "substitute(Bianchi2, domega, repeat=True)\n",
    "distribute(_)\n",
    "rename_dummies(_);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above result, when written in tensorial components, is the well-known _differential_ Bianchi identity:\n",
    "$$\\begin{equation*}\n",
    "  R^\\mu{}_{\\nu\\lambda\\rho;\\sigma} + R^\\mu{}_{\\nu\\sigma\\lambda;\\rho} + R^\\mu{}_{\\nu\\rho\\sigma;\\lambda} = 0.\n",
    "\\end{equation*}$$"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Cadabra",
   "language": "python",
   "name": "cadabra"
  },
  "language_info": {
   "file_extension": ".cdb",
   "mimetype": "text/cadabra",
   "name": "cadabra",
   "version": "2.0.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
