{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Mattia Scomparin\n",
    "mattia.scompa@gmail.com\n",
    "\n",
    "This notebook derives the Einstein equations by starting from the Einstein-Hilbert action, and requiring that its variational derivative with respect to the metric vanishes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cdb.core.manip as manip"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We first of all need to declare the indices which we will use, as well as the symbols we use for the metric and Kronecker delta."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\\mu,\\nu,\\rho,\\sigma,\\alpha,\\beta,\\gamma,\\lambda,\\tau,\\pi,\\xi,\\iota,\\omega,\\kappa,\\psi,\\chi,\\epsilon,\\upsilon}::Indices(position=independent);\n",
    "g_{\\mu\\nu}::Metric.\n",
    "g^{\\mu\\nu}::InverseMetric.\n",
    "{\\delta{#},g_{\\mu}^{\\nu},g^{\\mu}_{\\nu}}::KroneckerDelta."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make various symbols display in a more readable form,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\\Lm::LaTeXForm(\"\\mathcal{L}_{\\text{mat}}\").\n",
    "\\Dg::LaTeXForm(\"\\sqrt{-g}\").\n",
    "\\dg{#}::LaTeXForm(\"\\delta g\").\n",
    "\\dLm::LaTeXForm(\"\\delta\\mathcal{L}_{\\text{mat}}\").\n",
    "\\d::LaTeXForm(\"\\delta\").\n",
    "\\dDg::LaTeXForm(\"\\delta\\sqrt{-g}\").\n",
    "\\dCn{#}::LaTeXForm(\"\\delta\\Gamma\").\n",
    "\\dR{#}::LaTeXForm(\"\\delta R\")."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Christoffel symbols and their variations are symmetric in the two lower indices,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\\Gamma^{\\alpha}_{\\mu\\nu}::TableauSymmetry(shape={2}, indices={1,2}).\n",
    "\\dGamma^{\\alpha}_{\\mu\\nu}::TableauSymmetry(shape={2}, indices={1,2})."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need to declare which symbols to use for derivatives,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\\partial{#}}::PartialDerivative;\n",
    "{\\nabla{#},\\delta{#}}::Derivative;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now need to define the Christoffel connection in terms of the metric, and the Riemann tensor in terms of the Christoffel connection, as well as the Ricci tensor and scalar and Einstein tensor. These are also available in a Cadabra package, but for clarity we spell them out here.\n",
    "\n",
    "${}\\text{Attached property PartialDerivative to~}\\partial{\\#}.$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "connection       := \\Gamma^{\\mu}_{\\nu\\rho} = 1/2 g^{\\mu\\sigma} ( \\partial_{\\rho}{g_{\\nu\\sigma}} +\\partial_{\\nu}{g_{\\rho\\sigma}} -\\partial_{\\sigma}{g_{\\nu\\rho}} );\n",
    "riemannTensor    := R^{\\rho}_{\\sigma\\mu\\nu} = \\partial_{\\mu}{\\Gamma^{\\rho}_{\\nu\\sigma}} -\\partial_{\\nu}{\\Gamma^{\\rho}_{\\mu\\sigma}} \n",
    "                                              +\\Gamma^{\\rho}_{\\mu\\lambda} \\Gamma^{\\lambda}_{\\nu\\sigma} -\\Gamma^{\\rho}_{\\nu\\lambda} \\Gamma^{\\lambda}_{\\mu\\sigma};\n",
    "ricciTensor      := R_{\\sigma\\nu} = R^{\\rho}_{\\sigma\\rho\\nu};\n",
    "scalarCurvature  := R = R_{\\sigma\\nu} g^{\\sigma\\nu};      \n",
    "einsteinTensor   := G_{\\mu\\nu} = R_{\\mu\\nu} - 1/2 g_{\\mu\\nu} R;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are now ready to address the variational problem. The action we use is the Einstein-Hilbert action, amended with a cosmological constant term."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "action := S = \\int{\\Dg*[1/2*1/\\kappa*(R-2\\Lambda)+\\Lm]}{x};\n",
    "distribute(_);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we vary this, we need to construct the variation of the various objects in this action step by step. The variation of the metric determinant is given by"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "deltaMetricDeterminant := \\delta(\\Dg) = -\\frac{1}{2}*\\Dg*g_{\\mu\\nu}\\dg^{\\mu\\nu};"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By definition the variation of the matter Lagrangian gives the stress tensor; we rewrite it here slightly for later purposes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "energyMomentumTensor := \\Dg T_{\\mu\\nu}\\dg^{\\mu\\nu} = -2\\delta(\\Dg*\\Lm);\n",
    "product_rule(_)\n",
    "distribute(_)\n",
    "substitute(_,deltaMetricDeterminant)\n",
    "matterLagrangianVariation = manip.swap_sides(_);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next comes the variation of the Christoffel symbols. We then rewrite these in such a way that we isolate the derivative of the variation, as that object will appear once we start varying the Riemann tensor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "covariantDerivativeCn := \\nabla_{\\sigma}(\\dCn^{\\mu}_{\\nu\\rho}) = \\partial_{\\sigma}(\\dCn^{\\mu}_{\\nu\\rho})\n",
    "                        +\\Gamma^{\\mu}_{\\sigma\\alpha}\\dCn^{\\alpha}_{\\nu\\rho}-\\Gamma^{\\alpha}_{\\sigma\\nu}\\dCn^{\\mu}_{\\alpha\\rho}\n",
    "                        -\\Gamma^{\\alpha}_{\\sigma\\rho}\\dCn^{\\mu}_{\\nu\\alpha}; \n",
    "covariantDerivativeCn1 = manip.to_rhs(_, $\\nabla_{\\sigma}{A??}$);\n",
    "covariantDerivativeCn2 = manip.to_lhs(_, $\\partial_{\\sigma}{A??}$);\n",
    "partialDerivativeCn = manip.multiply_through(covariantDerivativeCn2, $-1$);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The variation of the Riemann tensor comes next. With a few steps this is rewritten to a form which only involves covariant derivatives of the variation of the Christoffel connection."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "deltaRiemannTensor = vary(riemannTensor, $\\Gamma^{\\mu}_{\\nu\\rho}->\\dCn^{\\mu}_{\\nu\\rho}, R^{\\rho}_{\\sigma\\mu\\nu} -> \\dR^{\\rho}_{\\sigma\\mu\\nu}$);\n",
    "sort_product(_);\n",
    "substitute(_,partialDerivativeCn);\n",
    "rename_dummies(_);\n",
    "canonicalise(_);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The variation of the Ricci tensor leads to the so-called Palatini identity:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "deltaRicciTensor = vary(ricciTensor, $R^{\\rho}_{\\sigma\\rho\\nu}->\\dR^{\\rho}_{\\sigma\\rho\\nu}, R_{\\sigma\\nu} -> \\dR_{\\sigma\\nu}$);\n",
    "substitute(_,deltaRiemannTensor);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now vary the scalar curvature."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "deltaScalarCurvature = vary(scalarCurvature, $R-> \\dR, R_{\\sigma\\nu} -> \\dR_{\\sigma\\nu}, g^{\\mu\\nu}->\\dg^{\\mu\\nu}$);\n",
    "substitute(_,deltaRicciTensor);\n",
    "distribute(_);\n",
    "substitute(_,$\\nabla_{\\sigma}(\\dCn^{\\mu}_{\\nu\\rho})g^{\\alpha\\beta}->\\nabla_{\\sigma}(\\dCn^{\\mu}_{\\nu\\rho}g^{\\alpha\\beta})$);\n",
    "canonicalise(_);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With these ingredients we can now vary the action: simply vary all ingredients and then substitute what we have found above for the variations of the metric, connection and curvature tensors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vary(action, $\\Dg->\\delta(\\Dg), R -> \\dR, \\Lm->\\delta(\\Lm), S->\\delta(S)$);\n",
    "substitute(action, deltaMetricDeterminant);\n",
    "substitute(action, matterLagrangianVariation);\n",
    "substitute(action, deltaScalarCurvature);\n",
    "distribute(action);\n",
    "rename_dummies(action);\n",
    "factor_out(action, $\\dg^{\\mu\\nu}, \\Dg$);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The terms proportional to $\\delta g^{\\mu\\nu}$ are the Einstein equations, the rest is a total derivative. We can rewrite this a bit further to get the familiar form:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t1 = action[1][0][0][2]\n",
    "eom:= 2\\kappa @(t1) = 0;\n",
    "distribute(_)\n",
    "collect_factors(_)\n",
    "manip.to_rhs(_, $- \\kappa T_{\\mu\\nu}$);\n",
    "substitute(_, manip.swap_sides(einsteinTensor));"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Cadabra",
   "language": "python",
   "name": "cadabra"
  },
  "language_info": {
   "file_extension": ".cdb",
   "mimetype": "text/cadabra",
   "name": "cadabra",
   "version": "2.0.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
