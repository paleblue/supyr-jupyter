### Scala
ENV SCALA_VERSION 2.12.10
ENV ALMOND_VERSION 0.9.1
RUN cd /opt && \
    mkdir almond && \
    pushd /opt/almond && \
	curl -Lo coursier https://git.io/coursier-cli && \
	chmod +x coursier && \
	./coursier bootstrap -r jitpack \
	    -i user -I user:sh.almond:scala-kernel-api_$SCALA_VERSION:$ALMOND_VERSION \
	    sh.almond:scala-kernel_$SCALA_VERSION:$ALMOND_VERSION \
	    -o almond && \
	./coursier fetch org.plotly-scala::plotly-almond:0.7.0 && \
	./coursier fetch --sources org.plotly-scala::plotly-almond:0.7.0 && \
	./almond --install && \
    popd
